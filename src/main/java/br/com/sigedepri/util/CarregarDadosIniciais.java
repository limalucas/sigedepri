package br.com.sigedepri.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.enuns.AtivoInativo;
import br.com.sigedepri.enuns.TipoPerfil;
import br.com.sigedepri.enuns.TipoTransacao;


public class CarregarDadosIniciais {

	private EntityManager entityManager;
	
	private Categoria transporte = null;
	
	private Categoria alimentacao = null;

	private Usuario lucas;

	private Categoria estacionamento;
	
	
	public static void main(String[] args) {
		CarregarDadosIniciais init = new CarregarDadosIniciais();
		init.init();
	}
	
	public void init() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("SiGeDePriPU");
		entityManager = factory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		Query createQuery = entityManager.createQuery("DELETE FROM Transacao");
		Query createQuery2 = entityManager.createQuery("DELETE FROM Categoria");
		Query createQuery3 = entityManager.createQuery("DELETE FROM Usuario");
		
		createQuery.executeUpdate();
		createQuery2.executeUpdate();
		createQuery3.executeUpdate();
		
		if(entityManager.createQuery("from Usuario", Usuario.class).getResultList().isEmpty()){
			incluirUsuario();
		}
		
		if(entityManager.createQuery("from Categoria", Categoria.class).getResultList().isEmpty()){
			incluirCategorias();
		}
		
		if(entityManager.createQuery("from Transacao", Transacao.class).getResultList().isEmpty()){
			try {
				incluirTransacoes();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		entityManager.getTransaction().commit();
	}
	
	private void incluirUsuario() {
		
		lucas = new Usuario();
		lucas.setEmail("lucaslima");
		lucas.setNome("Lucas Lima");
		lucas.setSenha("1");
		lucas.setPerfil(TipoPerfil.ADMINISTRADOR);
		lucas.setStatus(AtivoInativo.ATIVO); 
		
		entityManager.persist(lucas);
		
	}


	private void incluirTransacoes() throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString = "25-09-2016";
		Date date = sdf.parse(dateInString); 
		
		Transacao tr1 = new Transacao();
		tr1.setCategoria(transporte);
		tr1.setDescricao("Gasolina");
		tr1.setTipo(TipoTransacao.CREDITO);
		tr1.setValor(new BigDecimal(80.00));  
		tr1.setDataTransacao(date);
		tr1.setUsuario(lucas); 
		entityManager.persist(tr1);
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString2 = "26-09-2016";
		Date date2 = sdf2.parse(dateInString2); 
		
		Transacao tr2 = new Transacao();
		tr2.setCategoria(transporte);
		tr2.setDescricao("Gasolina");
		tr2.setTipo(TipoTransacao.CREDITO);
		tr2.setValor(new BigDecimal(50.00));  
		tr2.setDataTransacao(date2);
		tr2.setUsuario(lucas); 
		entityManager.persist(tr2);
		
		SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString3 = "15-09-2016";
		Date date3 = sdf3.parse(dateInString3); 
		
		Transacao tr3 = new Transacao();
		tr3.setCategoria(transporte);
		tr3.setDescricao("Gasolina");
		tr3.setTipo(TipoTransacao.CREDITO);
		tr3.setValor(new BigDecimal(60.00));  
		tr3.setDataTransacao(date3);
		tr3.setUsuario(lucas); 
		entityManager.persist(tr3);
		
		SimpleDateFormat sdf4 = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString4 = "10-09-2016";
		Date date4 = sdf4.parse(dateInString4); 
		
		Transacao tr4 = new Transacao();
		tr4.setCategoria(alimentacao);
		tr4.setDescricao("COMIDA DO TCE");
		tr4.setTipo(TipoTransacao.CREDITO);
		tr4.setValor(new BigDecimal(11.50));  
		tr4.setDataTransacao(date4);
		tr4.setUsuario(lucas); 
		entityManager.persist(tr4);
		
		SimpleDateFormat sdf5 = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString5 = "10-02-2017";
		Date date5 = sdf5.parse(dateInString5); 
		
		Transacao tr5 = new Transacao();
		tr5.setCategoria(transporte);
		tr5.setDescricao("IPVA");
		tr5.setTipo(TipoTransacao.DEBITO);
		tr5.setValor(new BigDecimal(206.50));  
		tr5.setDataTransacao(date5);
		tr5.setUsuario(lucas); 
		entityManager.persist(tr5);
		
		SimpleDateFormat sdf6 = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString6 = "15-02-2017";
		Date date6 = sdf6.parse(dateInString6); 
		
		Transacao tr6 = new Transacao();
		tr6.setCategoria(transporte);
		tr6.setDescricao("Estacionamento do Shopping 3 Américas");
		tr6.setTipo(TipoTransacao.DEBITO);
		tr6.setValor(new BigDecimal(7.00));  
		tr6.setDataTransacao(date6);
		tr6.setUsuario(lucas); 
		entityManager.persist(tr6);
	}


	private void incluirCategorias() {
		//Categorias superiores
		alimentacao = new Categoria("ALIMENTAÇÃO", "TODO TIPO DE DESPESAS COM COMIDA, PODE SER SUPERMERCADO, RESTAURANTE OU SALGADINHO", lucas);
		entityManager.persist(alimentacao);
		
		Categoria moradia = new Categoria("MORADIA", "TODO TIPO DE DESPESAS COM MORADIA, EX: ELETRICIDADE, MANUTENÇÃO DA CASA, ÁGUA, COMPRA DE APARELHOS DOMESTICOS", lucas);
		entityManager.persist(moradia);
		
		Categoria outros = new Categoria("OUTROS", "CATEGORIA GERAL", lucas);
		entityManager.persist(outros);
		
		transporte = new Categoria("TRANSPORTE", "TODO TIPO DE DESPESAS COM TRANSPORTE, EX: SEGURO DO CARRO, MANUTENÇÃO DO CARRO, CONSÓRCIOS E FINANCIAMENTO, COMBUSTÍVEL", lucas);
		entityManager.persist(transporte);
		
		Categoria educacao = new Categoria("EDUCAÇÃO", "TODO TIPO DE DESPESAS COM ESTUDOS OU EDUCAÇÃO, EX: CURSOS ONLINE, LIVROS, FACULDADE, MATERIAL ESCOLAR", lucas);
		entityManager.persist(educacao);
		
		Categoria saude = new Categoria("SAÚDE", "TODO TIPO DE DESPESAS COM SAÚDE, EX: REMÉDIOS, PLANO DE SAÚDE, EXAMES..", lucas);
		entityManager.persist(saude);
		
		Categoria lazer = new Categoria("LAZER", "TODO TIPO DE DESPESAS COM LAZER, EX: PESCARIA, VIAGENS, HOTEL..", lucas);
		entityManager.persist(lazer);
		
		Categoria doacao = new Categoria("DOAÇÃO", "TODO TIPO DE DESPESAS DE DOAÇÕES, EX: DIZIMO, OFERTAS, MISSÕES..", lucas);
		entityManager.persist(doacao);
		
		Categoria impostos = new Categoria("IMPOSTOS", "TODO TIPO DE DESPESAS COM IMPOSTOS, DAF, INSS, E ETC..", lucas);
		entityManager.persist(impostos);
		
		Categoria comunicacao = new Categoria("COMUNICAÇÃO", "TODO TIPO DE DESPESAS COM COMUNICAÇÃO, EX: CELULAR, INTERNET, TELEFONE RESIDENCIAL, APLICATIVOS", lucas);
		entityManager.persist(comunicacao);

		//SUBCATEGORIAS
		
		//TRANSPORTE
		
		Categoria combustivel = new Categoria("COMBUSTÍVEL", "TODO TIPO DE DESPESAS COM GASOLINA, ALCOOL E ECT", transporte, lucas);
		entityManager.persist(combustivel);
		
		estacionamento = new Categoria("ESTACIONAMENTO", "TODO TIPO DE DESPESAS COM ESTACIONAMENTO", transporte, lucas);
		entityManager.persist(estacionamento);
		
		Categoria mecanica = new Categoria("MECÂNICA", "TODO TIPO DE DESPESAS COM A MECÂNICA DO CARRO, EX: ÓLEO, PEÇAS E MÃO DE OBRA", transporte, lucas);
		entityManager.persist(mecanica);
		
		Categoria documentacao = new Categoria("DOCUMENTOS", "TODO TIPO DE DESPESAS COM DOCUMENTOS, DESPACHANTE E ETC", transporte, lucas);
		entityManager.persist(documentacao);
		
		Categoria seguro = new Categoria("SEGURO", "TODO TIPO DE DESPESAS COM SEGUROS", transporte, lucas);
		entityManager.persist(seguro);
		
		Categoria consorcio = new Categoria("CONSÓRCIO", "TODO TIPO DE DESPESAS COM CONSORCIO", transporte, lucas);
		entityManager.persist(consorcio);
		
		Categoria financiamento = new Categoria("FINANCIAMENTO", "TODO TIPO DE DESPESAS COM FINANCIAMENTO", transporte, lucas);
		entityManager.persist(financiamento);
		
		Categoria multa = new Categoria("MULTA", "TODO TIPO DE DESPESAS COM MULTAS", transporte, lucas);
		entityManager.persist(multa);
		
		//MORADIA
		
		Categoria energia = new Categoria("ELETRICIDADE", "TODO TIPO DE DESPESAS COM ELETRICIDADE", moradia, lucas);
		entityManager.persist(energia);
		
		Categoria condominio = new Categoria("CONDOMINIO", "TODO TIPO DE DESPESAS COM CONDOMINIO", moradia, lucas);
		entityManager.persist(condominio);
		
		Categoria internet = new Categoria("INTERNET", "TODO TIPO DE DESPESAS COM INTERNET RESIDENCIAL", comunicacao, lucas);
		entityManager.persist(internet);
		
		Categoria celular = new Categoria("CELULAR", "TODO TIPO DE DESPESAS COM TELEFONE CELULAR, COMO CAPINHA, CRÉDITO, PACOTES DE DADOS E ETC..", comunicacao, lucas);
		entityManager.persist(celular);
		
		Categoria skype = new Categoria("SKYPE", "TODO TIPO DE DESPESAS COM SKYPE", comunicacao, lucas);
		entityManager.persist(skype);
		
		//ALIMENTAÇÃO
		
		Categoria supermercado = new Categoria("SUPERMERCADO", "TODO TIPO DE DESPESAS COM SUPERMERCADO", alimentacao, lucas);
		entityManager.persist(supermercado);
		
		Categoria padaria = new Categoria("PADARIA", "TODO TIPO DE DESPESAS COM PADARIA, PÃO, BOLO, SALGADOS", alimentacao, lucas);
		entityManager.persist(padaria);
		
		Categoria restaurante = new Categoria("RESTAURANTE", "TODO TIPO DE DESPESAS COM RESTAURANTES SEJA NO PERIODO DA MANHÃ OU DA TARDE", alimentacao, lucas);
		entityManager.persist(restaurante);
		
		Categoria lanche = new Categoria("LANCHE", "TODO TIPO DE DESPESAS COM LANCHES, SALGADOS DA TARDE", alimentacao, lucas);
		entityManager.persist(lanche);
		
		Categoria suco = new Categoria("SUCO", "TODO TIPO DE DESPESAS COM SUCOS", alimentacao, lucas);
		entityManager.persist(suco);
		
		//SAÚDE
		
		Categoria unimed = new Categoria("UNIMED", "TODO TIPO DE DESPESAS COM PLANO DE SAÚDE", saude, lucas);
		entityManager.persist(unimed);
		
		Categoria remedio = new Categoria("REMÉDIO", "TODO TIPO DE DESPESAS COM REMÉDIOS", saude, lucas);
		entityManager.persist(remedio);
		
		//DOAÇÕES
		
		Categoria dizimo = new Categoria("DÍZIMO", "TODO TIPO DE DESPESAS COM DIZIMO", doacao, lucas);
		entityManager.persist(dizimo);
		
		Categoria oferta = new Categoria("OFERTA", "TODO TIPO DE DESPESAS COM OFERTAS PARA IGREJA OU PESSOAS CARENTES", doacao, lucas);
		entityManager.persist(oferta);
		
		//IMPOSTOS
		
		Categoria inss = new Categoria("INSS", "TODO TIPO DE DESPESAS COM INSS", impostos, lucas);
		entityManager.persist(inss);
		
		Categoria daf = new Categoria("DARF", "TODO TIPO DE DESPESAS COM DARF", impostos, lucas);
		entityManager.persist(daf);
		
		//EDUCAÇÃO
		
		Categoria facultade = new Categoria("FACULDADE", "TODO TIPO DE DESPESAS COM A FACULDADE", educacao, lucas);
		entityManager.persist(facultade);
		
		Categoria materialEscolar = new Categoria("MATERIAL ESCOLAR", "TODO TIPO DE DESPESAS COM MATERIAL ESCOLAR", educacao, lucas);
		entityManager.persist(materialEscolar);
		
		//LAZER
		
		Categoria eventosDaIgreja = new Categoria("EVENTO DA IGREJA", "QUALQUER TIPO DE EVENTO DA IGREJA, JANTARES, REUNIÕES, CONGRESSOS E ETC", lazer, lucas);
		entityManager.persist(eventosDaIgreja);
		
		//OUTROS
		
		Categoria outrossub = new Categoria("OUTROS SUB", "TODA TRANSAÇÃO QUE NÃO TENHA CATEGORIA DEFINIDA", outros, lucas);
		entityManager.persist(outrossub);
	}
	
	

	

}
