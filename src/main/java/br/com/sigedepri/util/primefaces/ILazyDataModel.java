package br.com.sigedepri.util.primefaces;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import br.com.sigedepri.entity.AbstractEntity;

public interface ILazyDataModel<K, E extends AbstractEntity<K>> {
	
	public K getRowKey(E object);
	public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);
	public E getRowData( String rowKey );

}
