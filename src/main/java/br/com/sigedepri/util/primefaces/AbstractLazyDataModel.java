package br.com.sigedepri.util.primefaces;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.sigedepri.dao.AbstractJpaDao;
import br.com.sigedepri.entity.AbstractEntity;

public class AbstractLazyDataModel<K,E extends AbstractEntity<K>> extends LazyDataModel<E> {

	private static final long serialVersionUID = 1L;
	
	private List<E> listaGenerica;
	
	private AbstractJpaDao<K,E> dao; 
	
	public AbstractLazyDataModel(AbstractJpaDao<K,E> dao) {
		this.dao = dao;
	} 

	@Override
	public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		System.out.println("PASSANDO PELA LAZY GENERICA");
		
		this.setRowCount(getDao().contaTodos()); 
		
		if ( this.getRowCount() > 0 ) {
			this.listaGenerica = getDao().listaTodosPaginada(first, pageSize);
		}
		
		//this.setPageSize(10);  

		return listaGenerica;
	}
	
	@Override
	public K getRowKey(E object) {
		return object.getId();
	}
	
	/*@Override
	@SuppressWarnings("unchecked")
	public E getRowData( String rowKey ) {
		final K id = (K) Integer.valueOf(rowKey);
		return getDao().buscaPorId(id);
	}*/
	
	@Override
	public E getRowData( String rowKey ) {
		Integer id = Integer.valueOf(rowKey);
		
		for (E e : this.getListaGenerica()) {
			if(id.equals(e.getId())){
				return e;
			}
		}
		
		return null;
	}

	public List<E> getListaGenerica() {
		return listaGenerica;
	}

	public AbstractJpaDao<K,E> getDao() {
		return dao;
	}



}
