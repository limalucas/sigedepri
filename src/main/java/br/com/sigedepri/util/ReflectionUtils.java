package br.com.sigedepri.util;

public class ReflectionUtils {
	
	/**
	 * Retorna uma nova instancia da classe
	 *
	 * @param clazz
	 *            Classe a ser instanciada
	 * @return Objeto instanciado, se houver construtor padrao.
	 */
	public static Object newInstance( Class<?> clazz ) {
		try {
			return clazz.newInstance();
		} catch ( InstantiationException | IllegalAccessException e ) {
			e.printStackTrace();
		}
		return null;
	}

}
