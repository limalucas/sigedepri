package br.com.sigedepri.util.converters;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.util.excpetion.ExceptionUtil;


@Named
@RequestScoped
@FacesConverter("abstractConverter")
public class AbstractConverter implements Converter {
	
	@Inject
	private EntityManager em;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		String id = null;
		String className = null;
		try {
			if (value.indexOf(":") > -1) {
				id = value.split(":")[1];
				className = value.split(":")[0];

				return em.find(Class.forName(className), Integer.valueOf(id));
			} 
		} catch (final Exception e) {
			throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro ao converter a entidade.", ExceptionUtil.getExceptionCause(e)), e);
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null &&  value instanceof AbstractEntity ) {
			final AbstractEntity entity = (AbstractEntity) value;
			if (entity.getId() != null) {
				return entity.getClass().getCanonicalName() + ":" + entity.getId().toString();
			}
		} 
		return null;
	}

}
