package br.com.sigedepri.util.env;

import br.com.sigedepri.enuns.EnvironmentType;

public class EnvironmentUtil {

	private static final String AMBIENTE = "ambiente";

	public static EnvironmentType getCurrentEviriomentType() {

		String ambiente = System.getProperty(AMBIENTE);
		EnvironmentType currentEnv = EnvironmentType.valueOf(ambiente);
		return currentEnv;

	}
}
