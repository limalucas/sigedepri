package br.com.sigedepri.util;

import java.io.Serializable;

public class TreeDocument implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Object object;
	
	private String tipo;
	
	private String dadosExtra;
	
	private String valorExtra;

	public TreeDocument() {
	}
	
	public TreeDocument(Object object, String tipo) {
		super();
		this.object = object;
		this.tipo = tipo;
	}

	public TreeDocument(Object object, String tipo, String dadosExtra, String valorExtra) {
		super();
		this.object = object;
		this.tipo = tipo;
		this.dadosExtra = dadosExtra;
		this.valorExtra = valorExtra;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDadosExtra() {
		return dadosExtra;
	}

	public void setDadosExtra(String dadosExtra) {
		this.dadosExtra = dadosExtra;
	}

	public String getValorExtra() {
		return valorExtra;
	}

	public void setValorExtra(String valorExtra) {
		this.valorExtra = valorExtra;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
