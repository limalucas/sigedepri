package br.com.sigedepri.util.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import br.com.sigedepri.util.excpetion.CoreException;
import br.com.sigedepri.util.excpetion.ExceptionUtil;


public final class FacesUtil implements SeverityType {

	public static boolean isPostback() {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.isPostback();
	}

	public static boolean isNotPostback() {
		return !isPostback();
	}

	public static void addMessage(String clientId, FacesMessage facesMessage) {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(clientId, facesMessage);
	}

	public static void addMessage(FacesMessage facesMessage) {
		addMessage(null, facesMessage);
	}

	public static void addMessage(String clientId, Severity severity, String summary, String detail) {
		addMessage(clientId, new FacesMessage(severity, summary, detail));
	}

	public static void addMessage(String clientId, String summary, String detail) {
		addMessage(clientId, new FacesMessage(summary, detail));
	}

	public static void addMessage(Severity severity, String summary, String detail) {
		addMessage(new FacesMessage(severity, summary, detail));
	}

	public static void addMessage(Severity severity, String summary) {
		addMessage(new FacesMessage(severity, summary, null));
	}

	public static void addMessage(String summary, String detail) {
		addMessage(new FacesMessage(summary, detail));
	}

	public static void addMessage(String summary) {
		addMessage(new FacesMessage(summary));
	}

	public static void addInfoMessage(String clientId, String summary, String detail) {
		addMessage(clientId, SEVERITY_INFO, summary, detail);
	}

	public static void addInfoMessage(String summary, String detail) {
		addMessage(SEVERITY_INFO, summary, detail);
	}

	public static void addInfoMessage(String summary) {
		addMessage(SEVERITY_INFO, summary);		
	}

	public static void addWarnMessage(String clientId, String summary, String detail) {
		addMessage(clientId, SEVERITY_WARN, summary, detail);
	}

	public static void addWarnMessage(String summary, String detail) {
		addMessage(SEVERITY_WARN, summary, detail);
	}

	public static void addWarnMessage(String summary) {
		addMessage(SEVERITY_WARN, summary);
	}

	public static void addErrorMessage(String clientId, String summary, String detail) {
		addMessage(clientId, SEVERITY_ERROR, summary, detail);
	}

	public static void addErrorMessage(String summary, String detail) {
		addMessage(SEVERITY_ERROR, summary, detail);
	}

	public static void addErrorMessage(String summary) {
		addMessage(SEVERITY_ERROR, summary);
	}

	public static void addFatalMessage(String clientId, String summary, String detail) {
		addMessage(clientId, SEVERITY_FATAL, summary, detail);
	}

	public static void addFatalMessage(String summary, String detail) {
		addMessage(SEVERITY_FATAL, summary, detail);
	}

	public static void addFatalMessage(String summary) {
		addMessage(SEVERITY_FATAL, summary);
	}

	public static Flash getFlash() {
		return getExternalContext().getFlash();	
	}

	public static ExternalContext getExternalContext() {
		return getFacesContext().getExternalContext();
	}

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	public static void handleException(Throwable exception) {
		if (exception instanceof CoreException) {
			final CoreException coreException = (CoreException) exception;
			addMessage(coreException.getSeverity(), coreException.getMessage(), coreException.getDetail());
		} else {
			addErrorMessage(ExceptionUtil.getExceptionCause(exception));
		}
	}

	public static void addFlashParameter(String key, Object value) {
		final Flash flash = getFlash();
		if (value instanceof FacesMessage) {
			flash.setKeepMessages(true);
		}
		flash.put(key, value);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getFlashParameter(Object key) {
		return (T) getFlash().get(key);
	}

	public static boolean hasFlashParameter(Object key) {
		return getFlash().containsKey(key);
	}

	public static void setKeepMessages(boolean flag){
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(flag);
	}


}
