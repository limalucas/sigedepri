package br.com.sigedepri.util.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

public interface SeverityType {
	
	public static final Severity SEVERITY_INFO = FacesMessage.SEVERITY_INFO;
	public static final Severity SEVERITY_WARN = FacesMessage.SEVERITY_WARN;
	public static final Severity SEVERITY_ERROR = FacesMessage.SEVERITY_ERROR;
	public static final Severity SEVERITY_FATAL = FacesMessage.SEVERITY_FATAL;

}
