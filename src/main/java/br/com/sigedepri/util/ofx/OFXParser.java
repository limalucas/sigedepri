package br.com.sigedepri.util.ofx;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.webcohesion.ofx4j.domain.data.MessageSetType;
import com.webcohesion.ofx4j.domain.data.ResponseEnvelope;
import com.webcohesion.ofx4j.domain.data.banking.BankStatementResponse;
import com.webcohesion.ofx4j.domain.data.banking.BankStatementResponseTransaction;
import com.webcohesion.ofx4j.domain.data.banking.BankingResponseMessageSet;
import com.webcohesion.ofx4j.domain.data.common.Transaction;
import com.webcohesion.ofx4j.domain.data.common.TransactionType;
import com.webcohesion.ofx4j.domain.data.creditcard.CreditCardResponseMessageSet;
import com.webcohesion.ofx4j.domain.data.creditcard.CreditCardStatementResponse;
import com.webcohesion.ofx4j.domain.data.creditcard.CreditCardStatementResponseTransaction;
import com.webcohesion.ofx4j.io.AggregateUnmarshaller;
import com.webcohesion.ofx4j.io.OFXParseException;

import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.enuns.TipoTransacao;


public class OFXParser {

	private static final String DEFAULT_CHARSET;
	private static final Pattern cardMemo;

	static {
		cardMemo = Pattern.compile("(Compra\\scom\\sCartão\\s-\\s\\d{2}/\\d{2}\\s\\d{2}:\\d{2}\\s)(.*)");
		DEFAULT_CHARSET = StandardCharsets.ISO_8859_1.displayName();
	}

	public static List<Transacao> parse(Reader reader) throws OFXParseException {
		List<Transacao> result = new ArrayList<Transacao>();

		AggregateUnmarshaller<ResponseEnvelope> unmarshaller = new AggregateUnmarshaller<ResponseEnvelope>(ResponseEnvelope.class);

		try {
			ResponseEnvelope envelope = unmarshaller.unmarshal(reader);
			BankingResponseMessageSet messageSetBanking = (BankingResponseMessageSet) envelope.getMessageSet(MessageSetType.banking);
			CreditCardResponseMessageSet messageSetCreditCard = (CreditCardResponseMessageSet) envelope.getMessageSet(MessageSetType.creditcard);
			
			if(messageSetBanking != null){
				List<BankStatementResponseTransaction> responses = messageSetBanking.getStatementResponses();
				for (BankStatementResponseTransaction response : responses) {
					BankStatementResponse message = response.getMessage();
					List<Transaction> transactions = message.getTransactionList().getTransactions();
					for (Transaction transaction : transactions) {
						result.add(buildOFXTransaction(transaction));
					}
				}
			}
			
			if(messageSetCreditCard != null){
				List<CreditCardStatementResponseTransaction> responses = messageSetCreditCard.getStatementResponses();
				for (CreditCardStatementResponseTransaction response : responses) {
					CreditCardStatementResponse message = response.getMessage();
					List<Transaction> transactions = message.getTransactionList().getTransactions();
					for (Transaction transaction : transactions) {
						result.add(buildOFXTransaction(transaction));
					}
				}
			}

			
		} catch (IOException e) {
			throw new OFXParseException(e);
		}

		return result;
	}

	public static List<Transacao> parse(InputStream stream) throws OFXParseException {
		try {
			return parse(new InputStreamReader(stream, DEFAULT_CHARSET));  
		} catch (IOException e) {
			e.printStackTrace();
			throw new OFXParseException(e); 
		}
	}
	
	/*private static String detectCharset(InputStream stream) throws IOException {		
		UniversalDetector detector = new UniversalDetector(null);
		
		String charset = null;
		try {
			byte[] buf = new byte[4096];
			int nread;
			while ((nread = stream.read(buf)) > 0 && !detector.isDone()) {
				detector.handleData(buf, 0, nread);
			}
			detector.dataEnd();

			charset = detector.getDetectedCharset();
		} catch (Exception e) {
			charset = null;
		} finally {
			stream.reset();
		}

		return charset == null ? DEFAULT_CHARSET : charset;
	}*/

	private static Transacao buildOFXTransaction(Transaction transaction) {
		String description = transaction.getMemo();
		Matcher matcher = cardMemo.matcher(description);
		
		if (matcher.matches()) {
			description = matcher.group(2);
		}
		
		TipoTransacao tipoTransacao = TipoTransacao.DEBITO; 
		
		if(transaction.getTransactionType().equals(TransactionType.CREDIT)){
			tipoTransacao = TipoTransacao.CREDITO;
		}
		
		if(transaction.getTransactionType().equals(TransactionType.DEBIT)){
			tipoTransacao = TipoTransacao.DEBITO;
		}

		return new Transacao(transaction.getId(), new BigDecimal(Math.abs(transaction.getAmount())), transaction.getDatePosted(), description, tipoTransacao);
	}
}
