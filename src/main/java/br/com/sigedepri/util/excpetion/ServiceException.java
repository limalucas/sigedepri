package br.com.sigedepri.util.excpetion;

import javax.faces.application.FacesMessage.Severity;

public class ServiceException extends CoreException {

	private static final long serialVersionUID = 1L;

	public ServiceException() {
		super();
	}

	public ServiceException(String message, Severity severity) {
		super(message, severity);
	}

	public ServiceException(String message, Throwable cause, Severity severity) {
		super(message, cause, severity);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(Throwable cause) {
		super(cause);
	}
	
	public ServiceException(String message, String detail) {
		super(message, detail);
	}
	
	public ServiceException(String message, Severity severity, String detail) {
		super(message, severity, detail);
	}
	
	public ServiceException(String message, Throwable cause, String detail) {
		super(message, cause, detail);
	}

}