package br.com.sigedepri.util.excpetion;

import javax.faces.application.FacesMessage.Severity;

/**
 * Esta classe representa exce��es que poder�o ser lan�adas pelo core.
 * 
 */
public class DaoException extends CoreException {

	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
	}

	public DaoException(String message, Severity severity) {
		super(message, severity);
	}

	public DaoException(String message, Throwable cause, Severity severity) {
		super(message, cause, severity);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

}