package br.com.sigedepri.util.excpetion;

import java.io.IOException;
import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.hibernate.exception.ConstraintViolationException;

import br.com.sigedepri.util.jsf.FacesUtil;


public class JsfExceptionHandler extends ExceptionHandlerWrapper {

	private ExceptionHandler wrapped;
	
	public JsfExceptionHandler(ExceptionHandler wrapped) {
		this.wrapped = wrapped;
	}
	
	@Override
	public ExceptionHandler getWrapped() {
		return this.wrapped;
	}
	
	@Override
	public void handle() throws FacesException {
		Iterator<ExceptionQueuedEvent> events = getUnhandledExceptionQueuedEvents().iterator();

		while (events.hasNext()) {
			ExceptionQueuedEvent event = events.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

			Throwable exception = context.getException();
			ConstraintViolationException constraintViolationException = ExceptionUtil.getTypedException(exception, ConstraintViolationException.class);
			CoreException coreException = ExceptionUtil.getTypedException(exception, CoreException.class);

			boolean handled = false;

			try {
				if (exception instanceof ViewExpiredException) {
					handled = true;
					FacesUtil.getFlash().setKeepMessages(true);
					FacesUtil.addErrorMessage("Sessão expirou!");
					redirect("/");
				} else if (constraintViolationException != null) {
					handled = true;
					String message = "Ocorreu uma violação de chave. ";
					if (constraintViolationException.getSQLState().equals("23000")) { //foreign_key_violation
						message += "Outro registro depende deste que está tentando remover. (%s)";
					} else if (constraintViolationException.getSQLState().equals("23505")) { //unique_violation
						message += "Verifíque se este registro já não está inserido. (%s)";
					}
					FacesUtil.addErrorMessage(String.format(message, constraintViolationException.getConstraintName()));
				} else if (coreException != null) {
					handled = true;
					FacesUtil.handleException(coreException);
				} else {
					handled = true;
					exception.printStackTrace();
					redirect("/error.xhtml");
				}
			} finally {
				if (handled) {
					events.remove();
				}
			}
		}

		getWrapped().handle();
	}
	
	
	private Throwable getLocalException(Throwable exception) {
		if (exception instanceof CoreException) {
			return exception;
		} else if (exception.getCause() != null) {
			return getLocalException(exception.getCause());
		}
		return null;
	}
	
	private void redirect(String page) {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			String contextPath = externalContext.getRequestContextPath();
	
			externalContext.redirect(contextPath + page);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

}
