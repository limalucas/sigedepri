package br.com.sigedepri.util.excpetion;

public final class ExceptionUtil {

	/**
	 * Obtém a causa da exceção.
	 * 
	 * @param exception
	 *            Exceção.
	 * @return String
	 */
	/*public static String getExceptionCause(Throwable exception) {
		if (exception != null) {
			return getExceptionCause(exception.getCause(), exception.getMessage());
		}
		return null;
	}

	private static String getExceptionCause(Throwable cause, String exceptionCause) {
		if (cause != null) {
			String newExceptionCause = (StringUtils.isEmpty(exceptionCause) ? "" : exceptionCause + "<br/>");
			newExceptionCause += (StringUtils.isEmpty(cause.getMessage()) ? "" : cause.getMessage());
			exceptionCause = getExceptionCause(cause.getCause(), newExceptionCause);
		}
		return exceptionCause;
	}*/
	public static String getExceptionCause(Throwable exception) {
		if (exception != null) {
			return getExceptionCause(exception.getCause(), exception.getMessage());
		}
		return null;
	}

	private static String getExceptionCause(Throwable cause, String exceptionCause) {
		if (cause != null) {
			String newExceptionCause = (isEmpty(exceptionCause) ? "" : exceptionCause + "<br/>");
			newExceptionCause += (isEmpty(cause.getMessage()) ? "" : cause.getMessage());
			exceptionCause = getExceptionCause(cause.getCause(), newExceptionCause);
		}
		return exceptionCause;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getTypedException(Throwable exception, Class<T> exceptionType) {
		if (exceptionType.isAssignableFrom(exception.getClass())) {
			return (T) exception;
		} else if (exception.getCause() != null) {
			return getTypedException(exception.getCause(), exceptionType);
		}
		return null;
	}
	
	public static boolean isEmpty(String value) {
		return value == null || value.trim().isEmpty();
	}
}
