package br.com.sigedepri.util.excpetion;

import javax.faces.application.FacesMessage.Severity;

public class BeanException extends CoreException {

	private static final long serialVersionUID = 1L;

	public BeanException() {
		super();
	}

	public BeanException(String message, Severity severity) {
		super(message, severity);
	}

	public BeanException(String message, Throwable cause, Severity severity) {
		super(message, cause, severity);
	}

	public BeanException(String message, Throwable cause) {
		super(message, cause);
	}

	public BeanException(String message) {
		super(message);
	}

	public BeanException(Throwable cause) {
		super(cause);
	}

}
