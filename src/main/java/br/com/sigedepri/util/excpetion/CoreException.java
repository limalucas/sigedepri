package br.com.sigedepri.util.excpetion;

import javax.ejb.ApplicationException;
import javax.faces.application.FacesMessage.Severity;

import br.com.sigedepri.util.jsf.SeverityType;

@ApplicationException(rollback=true)
public class CoreException extends Exception implements SeverityType {

	private static final long serialVersionUID = 1L;

	private Severity severity = SEVERITY_ERROR;
	private String detail = "";

	public CoreException() {
		super();
	}

	public CoreException(String message) {
		super(message);
	}
	
	public CoreException(String message, Severity severity) {
		super(message);
		setSeverity(severity);
	}

	public CoreException(Throwable cause) {
		super(cause);
		if (cause instanceof CoreException) {
			setSeverity(((CoreException) cause).getSeverity());
		}
	}

	public CoreException(String message, Throwable cause) {
		super(message, cause);
		if (cause instanceof CoreException) {
			setSeverity(((CoreException) cause).getSeverity());
		}
	}

	public CoreException(String message, Throwable cause, Severity severity) {
		super(message, cause);
		if (cause instanceof CoreException) {
			setSeverity(((CoreException) cause).getSeverity());
		} else {
			setSeverity(severity);
		}
	}
	
	public CoreException(String message, Severity severity, String detail) {
		super(message);
		setSeverity(severity);
		setDetail(detail);
	}
	
	public CoreException(String message, String detail) {
		super(message);
		setDetail(detail);
	}
	
	public CoreException(String message, Throwable cause, String detail) {
		super(message, cause);
		if (cause instanceof CoreException) {
			setSeverity(((CoreException) cause).getSeverity());
		}
		setDetail(detail);
	}
	
	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	

}
