package br.com.sigedepri.util;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	
	public static Date recuperaPrimeiroDiaDoMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, 0);
		aCalendar.set(Calendar.DATE, 1);
		return aCalendar.getTime();
	}
	
	public static Date recuperaUltimoDiaDoMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, 0);
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return aCalendar.getTime();
	}
	
	public static String retornaNomeDoMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		Locale ptBR = new Locale("pt", "BR");
		DateFormatSymbols symbols = new DateFormatSymbols(ptBR);
		String[] months = symbols.getMonths();
		return months[aCalendar.get(Calendar.MONTH)];
	}
	
	public static void main(String[] args) {
		
		String teste = "DECOLAR*33220 PARC 04/10 SAO PAULO";
		
		String[] split = teste.split("-");
		
		for (String string : split) {
			
			String teste1 = string.replaceAll("\\*", " ");
			
			System.out.println(teste1.trim());
		}
		
		
	}

}
