package br.com.sigedepri.entity;


public abstract class AbstractEntity<K> extends AbstractModel {

	private static final long serialVersionUID = 1L;
	
	public abstract K getId();

	public boolean isNewEntity() {
		return getId() == null;
	}

	@Override
	public int hashCode() {
		return getId() != null ? getId().hashCode() : super.hashCode();
	}

}
