package br.com.sigedepri.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "CATEGORIA")
public class Categoria extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "SEQ_CATEGORIA";

	@Id
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
	@Column(unique = true, nullable = false, precision = 8, name = "ID_CATEGORIA")
	private Integer id;
	
	@NotBlank(message="O nome é obrigatório")
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "CATEGORIA_PAI")
	private Categoria categoriaPai;
	
	@ManyToOne
	@JoinColumn(name= "USUARIO_ID")
	private Usuario usuario;
	
	@OneToMany(mappedBy="categoriaPai")
	private Set<Categoria> categorias = new HashSet<Categoria>();
	
	@OneToMany(mappedBy="categoria", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
	private List<PalavraChave> palavrasChaves;
	
	public Categoria() {
	}
	
	public Categoria(String nome, String descricao) {
		super();
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Categoria(String nome, String descricao,  Usuario usuario) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.usuario = usuario;
	}
	
	public Categoria(String nome, String descricao, Categoria categoriaPai) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.categoriaPai = categoriaPai;
	}
	
	public Categoria(String nome, String descricao, Categoria categoriaPai, Usuario usuario) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.categoriaPai = categoriaPai;
		this.usuario = usuario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getCategoriaPai() {
		return categoriaPai;
	}

	public void setCategoriaPai(Categoria categoriaPai) {
		this.categoriaPai = categoriaPai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Categoria))
			return false;
		Categoria other = (Categoria) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	public Set<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public PalavraChave addPalavraChave(PalavraChave palavraChave) {
		getPalavrasChaves().add(palavraChave);
		palavraChave.setCategoria(this);

		return palavraChave;
	}

	public PalavraChave removePalavraChave(PalavraChave palavraChave) {
		getPalavrasChaves().remove(palavraChave);
		palavraChave.setCategoria(null);

		return palavraChave;
	}

	public List<PalavraChave> getPalavrasChaves() {
		if( palavrasChaves == null ){
			return palavrasChaves = new ArrayList<>();
		}
		return palavrasChaves;
	}

	public void setPalavrasChaves(List<PalavraChave> palavrasChaves) {
		this.palavrasChaves = palavrasChaves;
	}

}
