package br.com.sigedepri.entity;

import java.math.BigDecimal;

public class BalancoDTO {

	private BigDecimal valor;

	private String categoria;

	private String porcentagem;

	public BalancoDTO(BigDecimal valor, String categoria) {
		super();
		this.valor = valor;
		this.categoria = categoria;
	}

	public BalancoDTO(BigDecimal valor, String categoria, String porcentagem) {
		super();
		this.valor = valor;
		this.categoria = categoria;
		this.porcentagem = porcentagem;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(String porcentagem) {
		this.porcentagem = porcentagem;
	}

}
