package br.com.sigedepri.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PALAVRA_CHAVE")
public class PalavraChave extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "SEQ_PALAVRA_CHAVE";

	@Id
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
	@Column(unique = true, nullable = false, precision = 8, name = "ID_PALAVRA_CHAVE")
	private Integer id;

	@Column(name = "CHAVE", unique = true)
	private String chave;

	@ManyToOne
	@JoinColumn(name = "CATEGORIA_ID")
	private Categoria categoria;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}
