package br.com.sigedepri.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.sigedepri.enuns.AtivoInativo;
import br.com.sigedepri.enuns.TipoPerfil;
import br.com.sigedepri.security.UsuarioFacebook;

@Entity
@Table(name = "USUARIO")
public class Usuario extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "SEQ_USUARIO";

	@Id
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
	@Column(unique = true, nullable = false, precision = 8, name = "ID_USUARIO")
	private Integer id;

	@Column(name = "email")
	private String email;

	private String nome;

	@Column(name = "senha")
	private String senha;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private AtivoInativo status;

	@Enumerated(EnumType.STRING)
	@Column(name = "perfil")
	private TipoPerfil perfil;
	
	@Column(name = "face_id")
	private Long facebookID;
	
	@Transient
	private UsuarioFacebook usuarioFacebook;
	
	public Usuario() {
	}

	public Usuario(UsuarioFacebook usuarioFacebook) {
		super();
		this.usuarioFacebook = usuarioFacebook;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoPerfil getPerfil() {
		return perfil;
	}

	public void setPerfil(TipoPerfil perfil) {
		this.perfil = perfil;
	}
	
	public boolean isAdministrador(){
		return getPerfil().equals(TipoPerfil.ADMINISTRADOR);
	}
	
	public boolean isGestor(){
		return getPerfil().equals(TipoPerfil.GESTOR_FINANCEIRO);
	}
	
	public boolean ehUsuarioFacebook(){
		return usuarioFacebook != null || facebookID != null;
	}

	public Long getFacebookID() {
		return facebookID;
	}

	public void setFacebookID(Long facebookID) {
		this.facebookID = facebookID;
	}

	public UsuarioFacebook getUsuarioFacebook() {
		return usuarioFacebook;
	}

	public void setUsuarioFacebook(UsuarioFacebook usuarioFacebook) {
		this.usuarioFacebook = usuarioFacebook;
	}

}
