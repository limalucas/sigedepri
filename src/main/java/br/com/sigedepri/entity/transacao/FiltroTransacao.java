package br.com.sigedepri.entity.transacao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.enuns.TipoConsulta;
import br.com.sigedepri.enuns.TipoTransacao;

public class FiltroTransacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final String TRANSACAO_FILTER = "transacaoFilter"; 

	private String descricao;
	private TipoTransacao tipoTransacao;
	private Categoria categoria;
	private Categoria subCategoria;
	private TipoConsulta tipoData;
	private Date dataInicial;
	private Date dataFinal;
	private TipoConsulta tipoValor;
	private BigDecimal valorInicial;
	private BigDecimal valorFinal;
	
	public FiltroTransacao() {
	}
	
	public FiltroTransacao(Date dataInical, Date dataFinal) {
		this.dataInicial = dataInical;
		this.dataFinal = dataFinal;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoTransacao getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacao tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Categoria getSubCategoria() {
		return subCategoria;
	}

	public void setSubCategoria(Categoria subCategoria) {
		this.subCategoria = subCategoria;
	}

	public TipoConsulta getTipoData() {
		return tipoData;
	}

	public void setTipoData(TipoConsulta tipoData) {
		this.tipoData = tipoData;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public TipoConsulta getTipoValor() {
		return tipoValor;
	}

	public void setTipoValor(TipoConsulta tipoValor) {
		this.tipoValor = tipoValor;
	}

	public BigDecimal getValorInicial() {
		return valorInicial;
	}

	public void setValorInicial(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}

	public BigDecimal getValorFinal() {
		return valorFinal;
	}

	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}
}
