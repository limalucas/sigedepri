package br.com.sigedepri.entity.transacao;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.enuns.TipoTransacao;
import br.com.sigedepri.util.Formatador;

	
@Entity
@Table(name = "TRANSACAO")
public class Transacao extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "SEQ_TRANSACAO";

	@Id
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
	@Column(unique = true, nullable = false, precision = 8, name = "ID_TRANSACAO")
	private Integer id;

	@NotBlank
	private String descricao;

	@NotNull
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date dataTransacao;

	@NotNull
	private BigDecimal valor;

	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoTransacao tipo;

	@ManyToOne
	@JoinColumn(name = "CATEGORIA_ID")
	private Categoria categoria;
	
	@ManyToOne
	@JoinColumn(name= "USUARIO_ID")
	private Usuario usuario;
	
	@Column(name = "idOFX")
	private String idOFX;
	
	@Transient
	private boolean selecionado;
	
	@Transient
	private Categoria categoriaPai;
	
	@Transient
	private List<Categoria> subCategorias;
	
	@Transient
	private boolean editando;
	
	public Transacao() {
	}

	public Transacao(String descricao, Date dataTransacao, BigDecimal valor) {
		super();
		this.descricao = descricao;
		this.dataTransacao = dataTransacao;
		this.valor = valor;
	}
	
	public Transacao(String idOFX, BigDecimal valor, Date dataTransacao, String descricao, TipoTransacao tipo) {
		super();
		this.descricao = descricao;
		this.dataTransacao = dataTransacao;
		this.valor = valor;
		this.idOFX = idOFX;
		this.tipo = tipo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}
	
	public String getDataTransacaoFormatada(){
		return Formatador.convertDateToStringSemHora(getDataTransacao());
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public BigDecimal getValor() {
		return valor;
	}
	
	public String getValorFormatado() {
		Locale ptBR = new Locale("pt", "BR");
		NumberFormat moedaFormat = NumberFormat.getCurrencyInstance(ptBR);
		return moedaFormat.format(getValor());
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoTransacao getTipo() {
		return tipo;
	}

	public void setTipo(TipoTransacao tipo) {
		this.tipo = tipo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public Categoria getCategoriaPai() {
		return categoriaPai;
	}

	public void setCategoriaPai(Categoria categoriaPai) {
		this.categoriaPai = categoriaPai;
	}

	public List<Categoria> getSubCategorias() {
		return subCategorias;
	}

	public void setSubCategorias(List<Categoria> subCategorias) {
		this.subCategorias = subCategorias;
	}
	
	public boolean isEditando(){
		//return getId() == null && editando;
		return editando;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getIdOFX() {
		return idOFX;
	}

	public void setIdOFX(String idOFX) {
		this.idOFX = idOFX;
	}

	public void setEditando( boolean editando ) {
		this.editando = editando;
	}

}
