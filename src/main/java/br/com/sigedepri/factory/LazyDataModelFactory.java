package br.com.sigedepri.factory;

import java.lang.reflect.ParameterizedType;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import br.com.sigedepri.annotation.Lazy;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.util.excpetion.CoreException;
import br.com.sigedepri.util.primefaces.AbstractLazyDataModel;

public class LazyDataModelFactory {
	
	
	@Produces @Lazy
	public <K,E extends AbstractEntity<K>> AbstractLazyDataModel<K,E> createLazyDataModel(InjectionPoint point) throws Exception {
		try { 
			System.out.println("FACTORY"); 
			
			ParameterizedType type = (ParameterizedType) point.getType();
			
			Class<E> classe = (Class<E>) type.getActualTypeArguments()[1];
			
			return null;
		} catch (Exception e) { 
			e.printStackTrace();
			throw new CoreException("Não foi possível obter uma nova instância da classe '" + getClass().getSimpleName() +  "'.", e);
		}
	}

}
