package br.com.sigedepri.factory;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import br.com.sigedepri.dao.IDao;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.util.excpetion.CoreException;

public class DAOFactory {
	
	@SuppressWarnings("unchecked")
	@Produces
	public <K,E extends AbstractEntity<K>> IDao<K,E> createDao(InjectionPoint point) throws Exception {
		try { 
			return (IDao<K, E>) getClass().newInstance();
		} catch (Exception e) { 
			e.printStackTrace();
			throw new CoreException("Não foi possível obter uma nova instância da classe '" + getClass().getSimpleName() +  "'.", e);
		}
	}

}
