package br.com.sigedepri.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import br.com.sigedepri.annotation.Configuration;


public class ConfigurationFactory {

	@Produces
	@Configuration
	@ApplicationScoped
	public Properties getProperties() throws IOException{
		InputStream inputStream = ConfigurationFactory.class.getResourceAsStream("/lucas-api.properties");
		
		Properties properties = new Properties();
		properties.load(inputStream);
		
		return properties;
	}
	
	
}