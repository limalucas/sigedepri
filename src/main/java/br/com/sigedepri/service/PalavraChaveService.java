package br.com.sigedepri.service;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.dao.PalavraChaveDAO;
import br.com.sigedepri.entity.PalavraChave;
import br.com.sigedepri.entity.Usuario;

public class PalavraChaveService extends AbstractService<Integer, PalavraChave, PalavraChaveDAO>{

	private static final long serialVersionUID = 1L;

	private PalavraChaveDAO dao;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	public boolean possuiPalavraChave(String chave){ 
		return dao.possuiPalavraChave(chave);
	}
	
	public PalavraChave recuperaPalavraChave(String chave){ 
		return dao.recuperaPalavraChave(chave);
	}
	
	@Inject
	public PalavraChaveService(PalavraChaveDAO dao) {
		this.dao = dao;
	}

}
