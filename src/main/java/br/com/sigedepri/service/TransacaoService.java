package br.com.sigedepri.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.webcohesion.ofx4j.io.OFXParseException;

import br.com.sigedepri.annotation.Transacional;
import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.dao.TransacaoDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.PalavraChave;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.ofx.OFXParser;

public class TransacaoService extends AbstractService<Integer, Transacao, TransacaoDAO>{

	private static final long serialVersionUID = 1L;
	
	private TransacaoDAO dao;
	
	@Inject
	private PalavraChaveService palavraChaveService;
	
	@Inject
	private CategoriaService categoriaService;

	public TransacaoService() {
	}
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	@Inject
	public TransacaoService(TransacaoDAO dao) {
		this.dao = dao; 
	}
	
	public Transacao recuperaTransacao(Transacao transacao) {
		return dao.recuperaTransacao(transacao); 
	}
	
	public List<Transacao> listaTransacoesPorCategoriaAndPeriodo(Categoria categoriaPai, Date dataInicial, Date dataFinal){
		return dao.listaTransacoesPorCategoriaAndPeriodo(categoriaPai, dataInicial, dataFinal);
	}
	
	public List<Transacao> listaTransacoesPorPeriodo(Date dataInicial, Date dataFinal){
		return dao.listaTransacoesPorPeriodo(dataInicial, dataFinal);
	}
	
	public List<Transacao> carregarArquivoEmLote(InputStream arquivo) throws ServiceException {
		List<Transacao> transacoes = null;
		try {
			transacoes = OFXParser.parse(arquivo);
			
			for (int i = 0; i < transacoes.size(); i++) {
				Transacao transacao = transacoes.get(i);
				
				if( dao.isTransacaoExistenteSemCategoria(transacao) ){
					transacao = dao.recuperaTransacao(transacao);
					transacao.setEditando(false); 
				} else { 
					transacao.setEditando(true);
					sugerirCategoria(transacao); 
				}
				
				transacoes.set(i, transacao);
			}
			
		} catch (OFXParseException e) { 
			throw new ServiceException("Não foi possível ler o arquivo.", e);
		}
		return transacoes;
	}
	
	private void sugerirCategoria(Transacao transacao){
		//primeiro pega pelo nome completo 
		if(palavraChaveService.possuiPalavraChave(transacao.getDescricao().trim())){
			 inserirDadosSugerido(transacao, transacao.getDescricao()); 
		} else {
			// tenta pegar por pedaços
			
			String splitRegex = " ";
			
			if( transacao.getDescricao().trim().contains("-") ){
				 splitRegex = "-";
			}
			
			
			String descricaoTratada = tratarCaracteresEspeciais(transacao.getDescricao());
			
			String[] descricoes = descricaoTratada.split(splitRegex);
			for (String chave : descricoes) {
				if(palavraChaveService.possuiPalavraChave(chave)){
					 inserirDadosSugerido(transacao, chave); 
					 break;
				}
			} 
		}
	}
	
	private String tratarCaracteresEspeciais(String descricao) {
		
		String descricaoSemEspacos = descricao.trim();
		
		String descricaoSemAsteristico = descricaoSemEspacos.replaceAll("\\*", " ");
		
		return descricaoSemAsteristico;
		
	}

	private void inserirDadosSugerido(Transacao transacao, String chave) {
		 PalavraChave palavraChave = palavraChaveService.recuperaPalavraChave(chave.trim());
		 
		 if( palavraChave == null ){
			 return;
		 }
		 
		 transacao.setCategoriaPai(palavraChave.getCategoria().getCategoriaPai()); 
		 transacao.setSubCategorias(this.categoriaService.listaSubCategorias(transacao.getCategoriaPai()));
		 transacao.setCategoria(palavraChave.getCategoria());
	}
	
	@Override
	@Transacional
	public Transacao salva(Transacao entity) throws br.com.sigedepri.util.excpetion.ServiceException {
		try {
			
			entity.getDescricao().trim();
			
			if(entity.getCategoriaPai() != null && entity.getCategoria() == null){
				entity.setCategoria( entity.getCategoriaPai() ); 
			} else if(entity.getCategoriaPai() != null && entity.getCategoria() != null){
				entity.setCategoria( entity.getCategoria() ); 
			} else {
				throw new ServiceException("Selecione uma categoria.");
			}
			
			if(dao.isTransacaoExistente(entity)){ 
				throw new ServiceException("Já existe um registro cadastrado com o mesmo nome e valor.");
			}
			
			entity.setUsuario( usuarioLogado.get() ); 
			
			return super.salva(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Não foi possível salvar o registro",e);
		} 
		
	}

}
