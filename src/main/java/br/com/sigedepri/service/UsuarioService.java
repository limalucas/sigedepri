package br.com.sigedepri.service;

import javax.inject.Inject;


import br.com.sigedepri.annotation.Transacional;
import br.com.sigedepri.dao.UsuarioDAO;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.enuns.AtivoInativo;
import br.com.sigedepri.enuns.TipoPerfil;
import br.com.sigedepri.util.excpetion.ServiceException;

public class UsuarioService extends AbstractService<Integer, Usuario, UsuarioDAO>{

	private static final long serialVersionUID = 1L;
	
	private UsuarioDAO dao;

	@Inject
	public UsuarioService(UsuarioDAO dao) {
		this.dao = dao;
	}
	
	@Transacional
	public Usuario salva(Usuario usuario) throws ServiceException {
		try{
			Usuario existe = dao.existe(usuario); 
			if(existe == null){
				if(usuario.ehUsuarioFacebook()){ 
					preencheUsuarioComDadosDoFacebook(usuario);
				}
				return dao.salva(usuario);
			}
			existe.setUsuarioFacebook(usuario.getUsuarioFacebook()); 
			return existe;
		}catch (Exception e) {
			throw new ServiceException("Não foi possível salvar o registro.", e);
		}
	}
	
	private void preencheUsuarioComDadosDoFacebook(Usuario usuario) {
		usuario.setFacebookID(usuario.getUsuarioFacebook().getId());
		usuario.setNome(usuario.getUsuarioFacebook().getName());
		usuario.setEmail(usuario.getUsuarioFacebook().getEmail()); 
		usuario.setPerfil(TipoPerfil.CONVIDADO); 
		usuario.setStatus(AtivoInativo.ATIVO);
	}

}
