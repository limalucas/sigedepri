package br.com.sigedepri.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import br.com.sigedepri.annotation.Transacional;
import br.com.sigedepri.dao.IDao;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.util.excpetion.DaoException;
import br.com.sigedepri.util.excpetion.ServiceException;


public abstract class AbstractService<K, E extends AbstractEntity<K>, D extends IDao<K, E>> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Instance<IDao<K, E>> dao;
	
	@SuppressWarnings("unchecked")
	protected D getDao() {
		return (D) dao.get();
	}
	
	@Transacional
	public E salva(E entity) throws ServiceException {
		try {
			return getDao().salva(entity);
		} catch (DaoException e) {
			e.printStackTrace();
			throw new ServiceException("Não foi possível salvar o registro", e);
		}
	}

	@Transacional
	public void removePorId(K id) throws ServiceException {
		try {
			getDao().removePorId(id);
		} catch (DaoException e) {
			e.printStackTrace();
			throw new ServiceException("Não foi possível remover o registro", e);
		} 
	} 
	
	@Transacional
	public void remove(E e) throws ServiceException {
		try {
			getDao().remove(e);
		} catch (DaoException e1) {
			e1.printStackTrace();
			throw new ServiceException("Não foi possível remover o registro", e1);
		} 
	}
	
	public List<E> listaTodos(){
		return getDao().listaTodos();
	}

}
