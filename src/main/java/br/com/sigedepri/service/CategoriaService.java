package br.com.sigedepri.service;

import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import br.com.sigedepri.annotation.Transacional;
import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.dao.CategoriaDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.util.excpetion.ServiceException;

public class CategoriaService extends AbstractService<Integer, Categoria, CategoriaDAO>{

	private static final long serialVersionUID = 1L;

	private CategoriaDAO dao;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	public CategoriaService() {
	}
	
	@Override
	@Transacional
	public Categoria salva(Categoria entity) throws ServiceException {

		entity.setUsuario(usuarioLogado.get()); 
		
		return super.salva(entity);
	}

	@Inject
	public CategoriaService(CategoriaDAO dao) {
		this.dao = dao;
	}
	
	public List<Categoria> listaCategoriasPorUsuario(){ 
		return dao.listaCategoriasPorUsuario();
	}

	public List<Categoria> listaCategoriasPai() {
		return dao.listaCategoriasPai();
	}

	public List<Categoria> listaSubCategorias(Categoria categoriaPai) {
		return dao.listaSubCategorias(categoriaPai);
	}

}
