package br.com.sigedepri.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import br.com.sigedepri.entity.BalancoDTO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.util.DateUtils;
import br.com.sigedepri.util.Formatador;

public class DashBoardService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private TransacaoService transacaoService;
	
	private CategoriaService categoriaService;

	private List<Categoria> listaCategoriasPai;

	private List<Transacao> listaTransacoesPorPeriodo;
	
	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	public DashBoardService() {
	}
	
	@Inject
	public DashBoardService(TransacaoService transacaoService, CategoriaService categoriaService) {
		this.transacaoService = transacaoService;
		this.categoriaService = categoriaService; 
	}
	
	@PostConstruct
	public void inicio(){
		this.listaCategoriasPai = categoriaService.listaCategoriasPai();
		this.listaTransacoesPorPeriodo = transacaoService.listaTransacoesPorPeriodo(DateUtils.recuperaPrimeiroDiaDoMesAtual(), DateUtils.recuperaUltimoDiaDoMesAtual());
	}
	
	public Map<Categoria, BigDecimal> recuperaBalancoMensalPorCategoriaAndSubCategoria(Date dataInicial, Date dataFinal){
		
		Map<Categoria, BigDecimal> balancoMensal = new HashMap<>();
		
		for (Categoria categoria : getListaCategoriasPai()) { 
			
			List<Transacao> listaTransacoesPorCategoriaAndPeriodo = recuperaListaDeTransacoesPorCategoriaAndPeriodo(categoria, dataInicial, dataFinal);
			BigDecimal valorToral = BigDecimal.ZERO;
			for (Transacao transacao : listaTransacoesPorCategoriaAndPeriodo) {
				valorToral = valorToral.add(transacao.getValor());
			}
			balancoMensal.put(categoria, valorToral);
		}
		
		return balancoMensal;
	}
	
	public List<Transacao> recuperaListaDeTransacoesPorCategoriaAndPeriodo(Categoria categoria, Date dataInicial, Date dataFinal){
		return transacaoService.listaTransacoesPorCategoriaAndPeriodo(categoria, dataInicial, dataFinal);
	}
	
	public Map<Categoria, BigDecimal> recuperaBalancoMensalPorCategoria(Date dataInicial, Date dataFinal){
		
		Map<Categoria, BigDecimal> balancoMensal = new HashMap<>();
		
		for (Categoria categoria : getListaCategoriasPai()) { 
			
			List<Transacao> listaTransacoesPorCategoriaAndPeriodo = transacaoService.listaTransacoesPorCategoriaAndPeriodo(categoria, dataInicial, dataFinal);
			BigDecimal valorToral = BigDecimal.ZERO;
			for (Transacao transacao : listaTransacoesPorCategoriaAndPeriodo) {
				valorToral = valorToral.add(transacao.getValor());
			}
			balancoMensal.put(categoria, valorToral);
		}
		
		return balancoMensal;
	}
	
	public String recuperaGastoTotalDoMesAtual(Date dataInicial, Date dataFinal){
		List<Transacao> listaTransacoesPorCategoriaAndPeriodo = transacaoService.listaTransacoesPorPeriodo(dataInicial, dataFinal);
		BigDecimal valorToral = BigDecimal.ZERO;
		for (Transacao transacao : listaTransacoesPorCategoriaAndPeriodo) {
			valorToral = valorToral.add(transacao.getValor());
		}
		
		return Formatador.formataNumero(valorToral);
	}
	
	public List<Transacao> recuperaGastosPorPeriodo(Date dataInicial, Date dataFinal){
		return transacaoService.listaTransacoesPorPeriodo(dataInicial, dataFinal);
	}
	
	public List<Categoria> getListaCategoriasPai() {
		return listaCategoriasPai;
	}

	public List<Transacao> getListaTransacoesPorPeriodo() {
		return listaTransacoesPorPeriodo;
	}

	public List<BalancoDTO> recuperaPorcentagem(Date dataInicial, Date dataFinal) { 
		
		List<BalancoDTO> listaDTO = new ArrayList<>();
		Map<Categoria, BigDecimal> balancoMensalPorCategoria = recuperaBalancoMensalPorCategoria(dataInicial, dataFinal);
		
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (Entry<Categoria, BigDecimal> entry : balancoMensalPorCategoria.entrySet()) {
			valorTotal = valorTotal.add(entry.getValue());
		}
		
		for (Entry<Categoria, BigDecimal> entry : balancoMensalPorCategoria.entrySet()) { 
			BigDecimal percentagePorCategoria = percentage(entry.getValue(),valorTotal);
			listaDTO.add(new BalancoDTO(entry.getValue(), entry.getKey().getNome(), percentagePorCategoria.toString())); 
		}
		
		Collections.sort(listaDTO, new Comparator<BalancoDTO>() {

			@Override
			public int compare(BalancoDTO o1, BalancoDTO o2) {
				return o2.getValor().compareTo(o1.getValor()); 
			}
		});
		
		return listaDTO; 
	}
	
	public BigDecimal percentage(BigDecimal base, BigDecimal total){
		if(total.equals(BigDecimal.ZERO)){
			return BigDecimal.ZERO;
		}
	    return base.multiply(ONE_HUNDRED).divide(total, 1, RoundingMode.HALF_UP);
	}

}
