package br.com.sigedepri.dao;

import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.TypedQuery;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.Usuario;
	
public class CategoriaDAO extends AbstractJpaDao<Integer,Categoria> {

	private static final long serialVersionUID = 1L;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	public List<Categoria> listaCategoriasPorUsuario(){ 
		TypedQuery<Categoria> query = getEntityManager().createQuery("select c from Categoria c where (c.usuario.id = :puserid or c.usuario.facebookID = :pfaceid) order by c.nome ASC", Categoria.class);
		query.setParameter("puserid", usuarioLogado.get().getId());
		query.setParameter("pfaceid", usuarioLogado.get().getFacebookID()); 
		return query.getResultList();
	}
	
	public List<Categoria> listaCategoriasPai(){ 
		TypedQuery<Categoria> query = getEntityManager().createQuery("select c from Categoria c where c.categoriaPai IS NULL and (c.usuario.id = :puserid or c.usuario.facebookID = :pfaceid) order by c.nome ASC", Categoria.class);
		query.setParameter("puserid", usuarioLogado.get().getId());
		query.setParameter("pfaceid", usuarioLogado.get().getFacebookID());  
		return query.getResultList();
	}
	
	public List<Categoria> listaSubCategorias(Categoria categoriaPai){
		TypedQuery<Categoria> query = getEntityManager().createQuery("select c from Categoria c where c.categoriaPai = :pcategoriaPai and (c.usuario.id = :puserid or c.usuario.facebookID = :pfaceid) order by c.nome ASC", Categoria.class);
		query.setParameter("pcategoriaPai", categoriaPai);
		query.setParameter("puserid", usuarioLogado.get().getId());
		query.setParameter("pfaceid", usuarioLogado.get().getFacebookID()); 
		return query.getResultList();
	}

}
