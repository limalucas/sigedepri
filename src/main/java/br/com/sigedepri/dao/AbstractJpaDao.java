package br.com.sigedepri.dao;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.util.excpetion.DaoException;

public abstract class AbstractJpaDao<K, E extends AbstractEntity<K>> implements IDao<K, E> {

	private static final long serialVersionUID = 1L;
	private static final String GLOBAL_FILTER = "globalFilter";

	@Inject
	private EntityManager entityManager;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	private CriteriaBuilder criteriaBuilder;
	@SuppressWarnings("rawtypes")
	private CriteriaQuery criteriaQuery;
	private Root<E> root;
	private List<Predicate> predicates = new ArrayList<Predicate>();
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init(){
		criteriaBuilder = entityManager.getCriteriaBuilder();
    	criteriaQuery = criteriaBuilder.createQuery();
    	root = criteriaQuery.from(getEntityClass());
    	predicates.add(criteriaBuilder.equal(criteriaBuilder.literal(1), 1));
	}

	@Override
	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass() {
		try {
			final ParameterizedType type = (ParameterizedType) getClass().getSuperclass().getGenericSuperclass();
			return (Class<E>) type.getActualTypeArguments()[0];
		} catch (Exception e) {
			final ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
			return (Class<E>) type.getActualTypeArguments()[1];
		}
	}

	@Override
	public E buscaPorId(K id) {
		E instancia = entityManager.find(getEntityClass(), id);
		return instancia;
	}
	
	@Override
	public Integer contaTodos() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		query.select(builder.count(query.from(getEntityClass())));
		Long result = entityManager.createQuery(query).getSingleResult();
		return result.intValue();
	}

	@Override
	public List<E> listaTodosPaginada(int firstResult, int maxResults) {
		CriteriaQuery<E> query = entityManager.getCriteriaBuilder().createQuery(getEntityClass());
		query.select(query.from(getEntityClass()));
		List<E> lista = entityManager.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
		return lista;
	}	
	
	@Override
	public List<E> consultaPaginada(int firstResult, int maxResults, Map<String, Object> filters) {
		TypedQuery<E> query = this.entityManager.createQuery(this.getQuery(filters));
		return query.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
	}
	
	@Override
    public Long count(Map<String, Object> filters) { 
        TypedQuery<Long> q = this.entityManager.createQuery(this.getQueryForCount(filters));
        return q.getSingleResult();
    }

	@Override
	public List<E> listaTodos() {
		CriteriaQuery<E> query = entityManager.getCriteriaBuilder().createQuery(getEntityClass());
		query.select(query.from(getEntityClass()));
		List<E> lista = entityManager.createQuery(query).getResultList();
		
		return lista;
	}

	@Override
	public E salva(E entity) throws DaoException {
		try {
			return entityManager.merge(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DaoException(e);
		}
	}

	@Override
	public void removePorId(K id) throws DaoException {
		try {
			E entity = buscaPorId(id); 
			entityManager.remove(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DaoException(e);
		}
	}
	
	@Override
	public void remove(E e) throws DaoException { 
		try {
			entityManager.remove(entityManager.merge(e));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new DaoException(ex);
		}
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@SuppressWarnings("unchecked")
	private CriteriaQuery<E> getQuery(Map<String, Object> filters) {
		this.criteriaQuery.select(this.root);
		includeWhereClause(filters); 
		this.criteriaQuery.where(predicates.toArray(new Predicate[]{}));
        return this.criteriaQuery;
    }

    @SuppressWarnings("unchecked")
    private CriteriaQuery<Long> getQueryForCount(Map<String, Object> filters) {
    	this.criteriaQuery.select(this.criteriaBuilder.count(this.root));
    	includeWhereClause(filters); 
    	this.criteriaQuery.where(predicates.toArray(new Predicate[]{}));
        return this.criteriaQuery;
    }
    
    protected void beforeFilter(Map<String, Object> filters){
    	if( usuarioLogado.get() != null ){		
    		Predicate usuarioPredicate  = criteriaBuilder.and(this.criteriaBuilder.equal(root.get("usuario").get("id"), usuarioLogado.get().getId()));
    		predicates.add(usuarioPredicate);
    	}
    }
    
	protected void includeWhereClause(Map<String, Object> filters) {
		
		beforeFilter(filters); 
		
		if(StringUtils.isNotBlank((String)filters.get(GLOBAL_FILTER))){     
			String filtroGlobal = (String)filters.get(GLOBAL_FILTER);
			List<Predicate> predicatesOr = new ArrayList<Predicate>();
			for( String fieldName : getStringValues()){		
				Predicate fieldsPredicate = this.criteriaBuilder.like(this.criteriaBuilder.upper(this.root.get(fieldName)), "%" + filtroGlobal.toUpperCase() + "%");
				predicatesOr.add(fieldsPredicate);
			}
			Predicate or = criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{}));			
			Predicate and = criteriaBuilder.and(or); 
			predicates.add(and); 
		}
	}
	
	private List<String> getStringValues() { 
		List<String> array = new ArrayList<>();
		for(Field f: getEntityClass().getDeclaredFields()) {            
		    if(f.getType().equals(String.class) && f.isAnnotationPresent(Column.class)) {              
		        f.setAccessible(true);
		        array.add(f.getName()); 
		    }           
		}
		return array;
	}

	public List<Predicate> getPredicates() {
		return predicates;
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return criteriaBuilder;
	}

	public Root<E> getRoot() {
		return root;
	}

}
