package br.com.sigedepri.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import br.com.sigedepri.entity.Usuario;

public class UsuarioDAO extends AbstractJpaDao<Integer,Usuario> { 

	private static final long serialVersionUID = 1L;
	
	public Usuario existe(Usuario usuario) {
		try {
			TypedQuery<Usuario> query = getEntityManager().createQuery("select u from Usuario u where u.email = :pEmail", Usuario.class);
			query.setParameter("pEmail", StringUtils.isBlank(usuario.getEmail()) ? usuario.getUsuarioFacebook().getEmail() : usuario.getEmail());
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
}
