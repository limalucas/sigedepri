package br.com.sigedepri.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.entity.transacao.FiltroTransacao;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.util.DateUtils;

public class TransacaoDAO extends AbstractJpaDao<Integer,Transacao>{

	private static final long serialVersionUID = 1L;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	public List<Transacao> listaTransacoesPorCategoriaAndPeriodo(Categoria categoriaPai, Date dataInicial, Date dataFinal){
		TypedQuery<Transacao> query = getEntityManager().createQuery("select t from Transacao t JOIN FETCH t.categoria c where (c.categoriaPai.id = :pcategoriapai or c.id = :pcategoriapai) and t.dataTransacao between :pdataInicio and :pdataFinal ", Transacao.class);
		query.setParameter("pcategoriapai", categoriaPai.getId());
		query.setParameter("pdataInicio", dataInicial);
		query.setParameter("pdataFinal", dataFinal);
		return query.getResultList();
	}
	
	public List<Transacao> listaTransacoesPorPeriodo(Date dataInicial, Date dataFinal){
		TypedQuery<Transacao> query = getEntityManager().createQuery("select t from Transacao t where t.dataTransacao between :pdataInicio and :pdataFinal ", Transacao.class);
		query.setParameter("pdataInicio", dataInicial);
		query.setParameter("pdataFinal", dataFinal);
		return query.getResultList();
	}
	
	public boolean isTransacaoExistente(Transacao transacao) {
		
		TypedQuery<Long> query = getEntityManager().createQuery("select count(t) from Transacao t where ((t.idOFX = :pidofx AND t.id != :pid) OR (UPPER(t.descricao) = UPPER(:pdescricao) and t.dataTransacao = :pdata and t.categoria.id = :pcategoria and t.valor = :pvalor AND t.id != :pid))", Long.class);
		query.setParameter("pdescricao", transacao.getDescricao());
		query.setParameter("pdata", transacao.getDataTransacao());
		query.setParameter("pcategoria", transacao.getCategoria().getId());
		query.setParameter("pvalor", transacao.getValor());
		query.setParameter("pidofx", transacao.getIdOFX());
		query.setParameter("pid", transacao.getId() != null ? transacao.getId() : 0); 
		BigDecimal result = new BigDecimal((Long)query.getSingleResult());
		return result.intValue() > 0;
	}
	
	public boolean isTransacaoExistenteSemCategoria(Transacao transacao) {
		
		TypedQuery<Long> query = getEntityManager().createQuery("select count(t) from Transacao t where ((t.idOFX = :pidofx AND t.id != :pid) OR (UPPER(t.descricao) = UPPER(:pdescricao) and t.dataTransacao = :pdata and t.valor = :pvalor AND t.id != :pid))", Long.class);
		query.setParameter("pdescricao", transacao.getDescricao());
		query.setParameter("pdata", transacao.getDataTransacao());
		query.setParameter("pvalor", transacao.getValor());
		query.setParameter("pidofx", transacao.getIdOFX());
		query.setParameter("pid", transacao.getId() != null ? transacao.getId() : 0); 
		BigDecimal result = new BigDecimal((Long)query.getSingleResult());
		return result.intValue() > 0;
	}
	
	public Transacao recuperaTransacao(Transacao transacao) {
		
		TypedQuery<Transacao> query = getEntityManager().createQuery("select t from Transacao t where ((t.idOFX = :pidofx AND t.id != :pid) OR (UPPER(t.descricao) = UPPER(:pdescricao) and t.dataTransacao = :pdata and t.valor = :pvalor AND t.id != :pid))", Transacao.class);
		query.setParameter("pdescricao", transacao.getDescricao());
		query.setParameter("pdata", transacao.getDataTransacao());
		query.setParameter("pvalor", transacao.getValor());
		query.setParameter("pidofx", transacao.getIdOFX());
		query.setParameter("pid", transacao.getId() != null ? transacao.getId() : 0); 
		return query.getSingleResult();
	}
	
	@Override
	protected void includeWhereClause(Map<String, Object> filters) {
		
		if(filters.get(FiltroTransacao.TRANSACAO_FILTER) != null){
			FiltroTransacao filtroTransacao = (FiltroTransacao) filters.get(FiltroTransacao.TRANSACAO_FILTER);
			
			if( StringUtils.isNoneBlank( filtroTransacao.getDescricao()) ){
				getPredicates().add( getCriteriaBuilder().like(getCriteriaBuilder().upper(getRoot().get("descricao")), "%" + filtroTransacao.getDescricao().toUpperCase() + "%") ); 
			}
			
			if( filtroTransacao.getTipoTransacao() != null ){
				getPredicates().add( getCriteriaBuilder().equal(getRoot().get("tipo"), filtroTransacao.getTipoTransacao())); 
			}
			
			if( filtroTransacao.getCategoria() != null && filtroTransacao.getSubCategoria() == null ){
				Predicate categoria = getCriteriaBuilder().equal(getRoot().get("categoria").get("id"), filtroTransacao.getCategoria().getId());
				Predicate subCategoria = getCriteriaBuilder().equal(getRoot().get("categoria").get("categoriaPai").get("id"), filtroTransacao.getCategoria().getId());
				
				Predicate or = getCriteriaBuilder().or(categoria,subCategoria);			
				Predicate and = getCriteriaBuilder().and(or); 
				getPredicates().add(and); 
			}
			
			if( filtroTransacao.getCategoria() != null && filtroTransacao.getSubCategoria() != null ){
				Predicate categoria = getCriteriaBuilder().equal(getRoot().get("categoria").get("id"), filtroTransacao.getSubCategoria().getId());
				Predicate subCategoria = getCriteriaBuilder().equal(getRoot().get("categoria").get("categoriaPai").get("id"), filtroTransacao.getSubCategoria().getId());
				
				Predicate or = getCriteriaBuilder().or(categoria,subCategoria);			
				Predicate and = getCriteriaBuilder().and(or); 
				getPredicates().add(and); 
			}
			
			if( filtroTransacao.getDataInicial() != null && filtroTransacao.getDataFinal() == null ){
				getPredicates().add( getCriteriaBuilder().equal(getRoot().get("dataTransacao"), filtroTransacao.getDataInicial())); 
			}
			
			if( filtroTransacao.getDataInicial() != null && filtroTransacao.getDataFinal() != null ){
				getPredicates().add( getCriteriaBuilder().between(getRoot().get("dataTransacao"), filtroTransacao.getDataInicial(), filtroTransacao.getDataFinal())); 
			}
			
			if( filtroTransacao.getValorInicial() != null && filtroTransacao.getValorFinal() == null ){
				getPredicates().add( getCriteriaBuilder().equal(getRoot().get("valor"), filtroTransacao.getDataInicial())); 
			}
			
			if( filtroTransacao.getValorInicial() != null && filtroTransacao.getValorFinal() != null ){
				getPredicates().add( getCriteriaBuilder().between(getRoot().get("valor"), filtroTransacao.getValorInicial(), filtroTransacao.getValorFinal())); 
			}
			
		} else {
			getPredicates().add( getCriteriaBuilder().between(getRoot().get("dataTransacao"), DateUtils.recuperaPrimeiroDiaDoMesAtual(), DateUtils.recuperaUltimoDiaDoMesAtual()));
		}
		
		super.includeWhereClause(filters);
	}

}
