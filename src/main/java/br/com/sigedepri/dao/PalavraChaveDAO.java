package br.com.sigedepri.dao;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.entity.PalavraChave;
import br.com.sigedepri.entity.Usuario;

public class PalavraChaveDAO extends AbstractJpaDao<Integer,PalavraChave> {

	private static final long serialVersionUID = 1L;
	
	@Inject @UsuarioLogado
	private Instance<Usuario> usuarioLogado;
	
	public boolean possuiPalavraChave(String chave){ 
		TypedQuery<Long> query = getEntityManager().createQuery("select count(p) from PalavraChave p where (p.categoria.usuario.id = :puserid or p.categoria.usuario.facebookID = :pfaceid) and UPPER(p.chave) = UPPER(:pchave)", Long.class);
		query.setParameter("pchave", chave.toUpperCase());
		query.setParameter("puserid", usuarioLogado.get().getId());
		query.setParameter("pfaceid", usuarioLogado.get().getFacebookID()); 
		Long result = query.getSingleResult();
		
		if(result.intValue() > 0 && result.intValue() < 2){
			return true;
		} 
		
		return false;
		
	}
	
	public PalavraChave recuperaPalavraChave(String chave){ 
		try{
			TypedQuery<PalavraChave> query = getEntityManager().createQuery("select p from PalavraChave p where (p.categoria.usuario.id = :puserid or p.categoria.usuario.facebookID = :pfaceid) and UPPER(p.chave) = UPPER(:pchave)", PalavraChave.class);
			query.setParameter("pchave", chave.toUpperCase());
			query.setParameter("puserid", usuarioLogado.get().getId());
			query.setParameter("pfaceid", usuarioLogado.get().getFacebookID()); 
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
