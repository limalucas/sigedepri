package br.com.sigedepri.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.util.excpetion.DaoException;


public interface IDao<K, E extends AbstractEntity<K>> extends Serializable {

	public Class<E> getEntityClass();
	public E buscaPorId(K id);
	public Integer contaTodos();
	public List<E> listaTodosPaginada(int firstResult, int maxResults);
	public List<E> consultaPaginada(int firstResult, int maxResults, Map<String, Object> filters);
	public List<E> listaTodos();
	public E salva(E entity) throws DaoException;
	public void remove(E id) throws DaoException;
	public void removePorId(K id) throws DaoException; 
	public Long count(Map<String, Object> filters);

}
