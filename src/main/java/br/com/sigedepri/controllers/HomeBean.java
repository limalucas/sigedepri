package br.com.sigedepri.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.PieChartModel;

import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.service.TransacaoService;
import br.com.sigedepri.util.Formatador;


@Named
@ViewScoped
public class HomeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PieChartModel gastoMensal;
	
	@Inject
	private TransacaoService transacaoService;
	
	@Inject
	private CategoriaService categoriaService;
	
	private Map<Categoria, BigDecimal> balancoMensal;

	@PostConstruct
	public void init() {
		createGastoMensal();
	}

	private void createGastoMensal() {
		gastoMensal = new PieChartModel();
		
		Map<Categoria, BigDecimal> balancoMensal = getBalancoMensal();
		for (Entry<Categoria, BigDecimal> entry : balancoMensal.entrySet()) {
			gastoMensal.set(entry.getKey().getNome(), entry.getValue());
		}
		
		gastoMensal.setTitle("Gasto Mensal");
		gastoMensal.setLegendPosition("w");
	}
	
	private void recuperaBalancoMensal(){
		
		this.balancoMensal = new HashMap<>();
		
		for (Categoria categoria : categoriaService.listaCategoriasPai()) {
			
			List<Transacao> listaTransacoesPorCategoriaAndPeriodo = transacaoService.listaTransacoesPorCategoriaAndPeriodo(categoria, recuperaPrimeiroDiaDoMesAtual(), recuperaUltimoDiaDoMesAtual());
			BigDecimal valorToral = BigDecimal.ZERO;
			for (Transacao transacao : listaTransacoesPorCategoriaAndPeriodo) {
				valorToral = valorToral.add(transacao.getValor());
			}
			balancoMensal.put(categoria, valorToral);
		}
	}
	
	public String recuperaGastoTotalDoMesAtual(){
		List<Transacao> listaTransacoesPorCategoriaAndPeriodo = transacaoService.listaTransacoesPorPeriodo(recuperaPrimeiroDiaDoMesAtual(), recuperaUltimoDiaDoMesAtual());
		BigDecimal valorToral = BigDecimal.ZERO;
		for (Transacao transacao : listaTransacoesPorCategoriaAndPeriodo) {
			valorToral = valorToral.add(transacao.getValor());
		}
		
		return Formatador.formataNumero(valorToral);
	}
	
	public String retornaMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		Locale ptBR = new Locale("pt", "BR");
		DateFormatSymbols symbols = new DateFormatSymbols(ptBR);
		String[] months = symbols.getMonths();
		return months[aCalendar.get(Calendar.MONTH)];
	}
	
	private Date recuperaPrimeiroDiaDoMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, 0);
		aCalendar.set(Calendar.DATE, 1);
		return aCalendar.getTime();
	}
	
	private Date recuperaUltimoDiaDoMesAtual(){
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, 0);
		aCalendar.set(Calendar.DATE, 1);

		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return aCalendar.getTime();
	}

	public PieChartModel getPieModel1() {
		return gastoMensal;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.gastoMensal = pieModel1;
	}

	public Map<Categoria, BigDecimal> getBalancoMensal() {
		if(balancoMensal == null){
			recuperaBalancoMensal();
		}
		return balancoMensal;
	}

	public void setBalancoMensal(Map<Categoria, BigDecimal> balancoMensal) {
		this.balancoMensal = balancoMensal;
	}

}
