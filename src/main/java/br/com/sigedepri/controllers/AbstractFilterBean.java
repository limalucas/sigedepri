package br.com.sigedepri.controllers;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.NoResultException;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.sigedepri.dao.IDao;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.service.AbstractService;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.jsf.FacesUtil;

public class AbstractFilterBean<K, E extends AbstractEntity<K>, D extends IDao<K, E>, S extends AbstractService<K, E, D>> extends AbstractBean<K, E, D, S> {

	private static final long serialVersionUID = 1L;

	private E objSelected;
	
	private E entidade;
	
	
	private List<E> lista;
	
	private LazyDataModel<E> lazyDataModel;
	
	@PostConstruct 
	public void init() {
	}
	
	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass() {
		try {
			final ParameterizedType type = (ParameterizedType) getClass().getSuperclass().getGenericSuperclass();
			return (Class<E>) type.getActualTypeArguments()[0];
		} catch (Exception e) {
			final ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
			return (Class<E>) type.getActualTypeArguments()[1];
		}
	}
	
	public void excluir() throws ServiceException { 
		getService().remove(objSelected);
		FacesUtil.addInfoMessage("Registro excluido com sucesso.");
	}

	public String editar() {
		FacesUtil.addFlashParameter(getObjSelected().getClass().getSimpleName().toLowerCase(), getObjSelected());
		return "/"+ getObjSelected().getClass().getSimpleName().toLowerCase() +"/cadastro"+getObjSelected().getClass().getSimpleName()+"?faces-redirect=true";
	}
	
	public E getObjSelected() {
		return objSelected;
	}

	public void setObjSelected(E objSelected) {
		this.objSelected = objSelected;
	}

	public E getEntidade() {
		return entidade;
	}

	public void setEntidade(E entidade) {
		this.entidade = entidade;
	}
	
	public LazyDataModel<E> createTable() {
		final LazyDataModel<E> dataModel = new LazyDataModel<E>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			@SuppressWarnings("unchecked")
			public E getRowData( String rowKey ) {
				final K id = (K) Integer.valueOf(rowKey);
				return getDao().buscaPorId(id);
			}

			@Override
			public Object getRowKey( E entity ) {
				return entity.getId();
			}

			@Override
			public List<E> load( int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {
				try {
					this.setRowCount(getDao().count(filters).intValue());
					if (this.getRowCount() > 0) {
						lista = getDao().consultaPaginada(first, pageSize, filters);
					}					
				} catch ( final NoResultException e ) {
					return null;
				} catch ( final Exception e ) {
					e.printStackTrace();
				}
				return lista;
			}

		};

		return dataModel;
	}

	public LazyDataModel<E> getLazyDataModel() {
		if ( this.lazyDataModel == null ) {
			lazyDataModel = createTable();
		}
		return lazyDataModel;
	}

	public void setLazyDataModel(LazyDataModel<E> lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}


}
