package br.com.sigedepri.controllers.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

import br.com.sigedepri.entity.BalancoDTO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.service.DashBoardService;
import br.com.sigedepri.util.DateUtils;
import br.com.sigedepri.util.Formatador;
import br.com.sigedepri.util.TreeDocument;
import br.com.sigedepri.util.jsf.FacesUtil;

@Named
@ViewScoped
public class DashBoardBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private PieChartModel gastoMensal;
	private LineChartModel dateModel;
	
	private Date dataInicial;
	private Date dataFinal;
	
	private String totalMensal;
	
	private TreeNode root;
	
	@Inject 
	private CategoriaService categoriaService;
	
	@Inject 
	private DashBoardService dashBoardService;
	
	@PostConstruct
	public void init(){
		if(FacesUtil.isNotPostback()){
			criaComponentes();
		}
	}
	
	public void criaComponentes(){
		recuperaTotalMensal();
		criaGastoMensalPizza();
		criaTreeTable();
		createDateModel();
	}
	
	private void criaTreeTable(){
		root = new DefaultTreeNode("Root", null);
		
		for (Categoria categoriaPai : dashBoardService.getListaCategoriasPai()) {
			
			BigDecimal valorTotalCategoria = BigDecimal.ZERO;
			for (Transacao transacao : dashBoardService.recuperaListaDeTransacoesPorCategoriaAndPeriodo(categoriaPai, getDataInicial(), getDataFinal())) {
				valorTotalCategoria = valorTotalCategoria.add(transacao.getValor());
			}
			
			DefaultTreeNode categoriaPaiNode = new DefaultTreeNode("categoria", new TreeDocument(categoriaPai, Formatador.formataNumero(valorTotalCategoria), "100", null), root); 
			categoriaPaiNode.setExpanded(true);   
			
			for (Categoria subCategoria : categoriaService.listaSubCategorias(categoriaPai)) {
				
				BigDecimal valorTotalSubCategoria = BigDecimal.ZERO;
				for (Transacao transacao : dashBoardService.recuperaListaDeTransacoesPorCategoriaAndPeriodo(subCategoria, getDataInicial(), getDataFinal())) {
					valorTotalSubCategoria = valorTotalSubCategoria.add(transacao.getValor());
				}
				
				BigDecimal porcento = dashBoardService.percentage(valorTotalSubCategoria, valorTotalCategoria);
				new DefaultTreeNode("subCategoria", new TreeDocument(subCategoria, Formatador.formataNumero(valorTotalSubCategoria), porcento.toString(), null), categoriaPaiNode);
			}
			
		}
	}

	private void criaGastoMensalPizza() {
		this.gastoMensal = new PieChartModel();
		
		Map<Categoria, BigDecimal> balancoMensal = dashBoardService.recuperaBalancoMensalPorCategoria(getDataInicial(), getDataFinal());
		for (Entry<Categoria, BigDecimal> entry : balancoMensal.entrySet()) {
			this.gastoMensal.set(entry.getKey().getNome(), entry.getValue());
		}
		
		this.gastoMensal.setTitle("Despesas por Categorias"); 
		this.gastoMensal.setLegendPosition("w"); 
	}
	
	private void createDateModel() {
        dateModel = new LineChartModel();
       
        List<Transacao> gastosPorPeriodo = dashBoardService.recuperaGastosPorPeriodo(getDataInicial(), getDataFinal());
        
        Collections.sort(gastosPorPeriodo, new Comparator<Transacao>() {
			@Override
			public int compare(Transacao o1, Transacao o2) {
				if (o1.getDataTransacao() == null || o2.getDataTransacao() == null){
			        return 0;
				}
			   return o1.getDataTransacao().compareTo(o2.getDataTransacao());
			}
		});
        
       /* int days = (int)( (getDataFinal().getTime() - getDataInicial().getTime()) / (1000 * 60 * 60 * 24));
        
        if( days == 30 || days < 31 ){
        	GregorianCalendar gregorianCalendar = new GregorianCalendar();
        	gregorianCalendar.setTime(getDataInicial());
        	int numberOfDays = gregorianCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        	
        	
        	
        }*/
        
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Series 1");
 
       /* Calendar start = Calendar.getInstance();
    	start.setTime(getDataInicial());
    	Calendar end = Calendar.getInstance();
    	end.setTime(getDataFinal());
    	
    	for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
    		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    		String dateS = fmt.format(date);
    		System.out.println(dateS); 
    		series1.set(dateS, date.getDay());
    	}*/
        
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        for (Transacao transacao : gastosPorPeriodo) {
    		String dateS = fmt.format(transacao.getDataTransacao());
    		series1.set(dateS, transacao.getValor().doubleValue());
		}
 
        dateModel.addSeries(series1);
         
        dateModel.setTitle("Valores Gastos");
        dateModel.setZoom(true);
        dateModel.getAxis(AxisType.Y).setLabel("Valores");
        DateAxis axis = new DateAxis("Datas");
        axis.setTickAngle(-50);
        axis.setMax("2017-05-31");
        axis.setTickFormat("%b %#d, %y");
         
        dateModel.getAxes().put(AxisType.X, axis);
    }
	
	public List<BalancoDTO> porcentagem(){
		return this.dashBoardService.recuperaPorcentagem(getDataInicial(), getDataFinal());
	}
	
	public void recuperaTotalMensal(){
		this.setTotalMensal(this.dashBoardService.recuperaGastoTotalDoMesAtual(getDataInicial(), getDataFinal()));
	}
	
	public String mesAtual(){
		return DateUtils.retornaNomeDoMesAtual();
	}

	public PieChartModel getGastoMensal() {
		return gastoMensal;
	}

	public void setGastoMensal(PieChartModel gastoMensal) {
		this.gastoMensal = gastoMensal;
	}

	public Date getDataInicial() {
		if(dataInicial == null){
			dataInicial = DateUtils.recuperaPrimeiroDiaDoMesAtual();
		}
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		if(dataFinal == null){
			dataFinal = DateUtils.recuperaUltimoDiaDoMesAtual();
		}
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getTotalMensal() {
		if(totalMensal == null){
			recuperaTotalMensal();
		}
		return totalMensal;
	}

	public void setTotalMensal(String totalMensal) {
		this.totalMensal = totalMensal;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	 public LineChartModel getDateModel() {
		 return dateModel;
	 }

}
