package br.com.sigedepri.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sigedepri.security.LoginFacebook;
import br.com.sigedepri.util.excpetion.CoreException;


@Named
@RequestScoped
public class LoginFaceRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value = "#{param.code}")
	private String code;
	
	@Inject
	private LoginFacebook loginFacebook;

	public void logarComFacebook() throws CoreException { 
		try {
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			externalContext.redirect(loginFacebook.getLoginRedirectURL());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CoreException("Não foi possível redirecional para o facebook!");
		}
	}
		
}
