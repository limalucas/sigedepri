package br.com.sigedepri.controllers;

import br.com.sigedepri.dao.IDao;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.service.AbstractService;
import br.com.sigedepri.util.jsf.FacesUtil;

public class AbstractFormBean<K, E extends AbstractEntity<K>, D extends IDao<K, E>, S extends AbstractService<K, E, D>> extends AbstractBean<K, E, D, S> {

	private static final long serialVersionUID = 1L;

	protected void salva(E entity) {
		try {
			getService().salva(entity);
			FacesUtil.addErrorMessage("Registro salvo com sucesso!");
		} catch (Exception e) {
			FacesUtil.handleException(e);
		}
	}

}
