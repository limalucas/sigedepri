package br.com.sigedepri.controllers;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sigedepri.dao.UsuarioDAO;
import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.util.jsf.SeverityType;


@Named
@ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	private String code;
	
	private Usuario usuario = new Usuario();

	public Usuario getUsuario() {
		return usuario;
	}

	public String efetuaLogin() {

		FacesContext context = FacesContext.getCurrentInstance();
		Usuario usuarioExiste = usuarioDAO.existe(this.usuario);

		if (usuarioExiste != null) {

			context.getExternalContext().getSessionMap().put("usuarioLogado", usuarioExiste);

			return "home?faces-redirect=true";
		}
		
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage(SeverityType.SEVERITY_ERROR, "Usuário não encontrado", "entre em contato com o administrador \n (65) 99620-5350"));

		return "login?faces-redirect=true";
	}
	
	public String deslogar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("usuarioLogado");
		
		return "/login?faces-redirect=true";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
