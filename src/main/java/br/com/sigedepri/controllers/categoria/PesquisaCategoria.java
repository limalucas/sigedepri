package br.com.sigedepri.controllers.categoria;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.sigedepri.controllers.AbstractFilterBean;
import br.com.sigedepri.dao.CategoriaDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.service.CategoriaService;

@Named
@ViewScoped
public class PesquisaCategoria extends AbstractFilterBean<Integer, Categoria, CategoriaDAO, CategoriaService> {

	private static final long serialVersionUID = 1L;
	

}
