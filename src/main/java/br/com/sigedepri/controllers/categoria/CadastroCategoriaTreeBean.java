package br.com.sigedepri.controllers.categoria;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import br.com.sigedepri.controllers.AbstractBean;
import br.com.sigedepri.dao.CategoriaDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.util.TreeDocument;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroCategoriaTreeBean extends AbstractBean<Integer, Categoria, CategoriaDAO, CategoriaService> {

	private static final long serialVersionUID = 1L;
	
	private Categoria categoriaSelecionado;
	
	private TreeNode root;
     
	private TreeNode selectedNode;
	
	@PostConstruct
	public void init(){
				
		if(FacesUtil.isNotPostback()){
			novoCategoria();
			criaTreeNode();
		}
	}
	
	public void novoCategoria(){ 
		categoriaSelecionado = new Categoria();
	}
	
	private void criaTreeNode(){
		this.root = new DefaultTreeNode("Root", null);
		
		for (Categoria categoria : getDao().listaTodos()) { 
			TreeNode categoriaPaiNode = null;
			
			if(categoria.getCategoriaPai() == null){
				categoriaPaiNode = new DefaultTreeNode(new TreeDocument(categoria, categoria.getNome()), root);
			}
			
			if(categoria.getCategorias() != null && !categoria.getCategorias().isEmpty()){
				for (Categoria categoriaFilho : categoria.getCategorias()) {
					DefaultTreeNode categoriaFilhoNode = new DefaultTreeNode(new TreeDocument(categoriaFilho, categoriaFilho.getNome()), categoriaPaiNode);
					for (Categoria categoriaNeta : categoriaFilho.getCategorias()) {
						 new DefaultTreeNode(new TreeDocument(categoriaNeta, categoriaNeta.getNome()), categoriaFilhoNode);
					}
				}
			}
		}
	}
	
	private void removeTreeNodeFromRoot(TreeNode node){
		List<TreeNode> nodes = getRoot().getChildren();
		Iterator<TreeNode> i = nodes.iterator();
		while (i.hasNext()) {
		   TreeNode next = i.next();  
		   
		   if(next.getChildren().contains(node)){
			   next.getChildren().remove(node);
		   }
		}
	}
	
	private Categoria getDataFromSelectedNode(TreeNode treeNode){
		TreeDocument document = (TreeDocument) getSelectedNode().getData();
		return (Categoria) document.getObject();
	}
	
	private boolean ehSelectedNode(){
		return getSelectedNode() != null;
	}
	
	private boolean isEditando(){
		return getCategoriaSelecionado().getId() != null;
	}
	
	public void salvar(){
		Categoria Categoria = null;
		try {
			// Alterando registro
			if(ehSelectedNode()){
				
				//editando
				if(isEditando()){
					
					Categoria = getService().salva(getCategoriaSelecionado());

					//inclui objeto novo
					new DefaultTreeNode(new TreeDocument(Categoria, Categoria.getNome()), getSelectedNode().getParent());
					
					//remove o objeto antigo
					removeTreeNodeFromRoot(getSelectedNode()); 
					
				} else {
					this.getCategoriaSelecionado().setCategoriaPai(this.getDataFromSelectedNode(getSelectedNode())); 
					Categoria novoCategoriaLeaf = getService().salva(this.getCategoriaSelecionado());
					new DefaultTreeNode(new TreeDocument(novoCategoriaLeaf, novoCategoriaLeaf.getNome()), getSelectedNode());
					this.getSelectedNode().setExpanded(true);
				}
			} else {
				// incluindo registro novo
				Categoria novoCategoriaPai = getService().salva(getCategoriaSelecionado());
				new DefaultTreeNode(new TreeDocument(novoCategoriaPai, novoCategoriaPai.getNome()), getRoot());
			}
			
			novoCategoria();
			setSelectedNode(null);
			final RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgCategoria').hide();");
			FacesUtil.addInfoMessage("Registro alterado com sucesso."); 
		} catch (ServiceException e) {
			e.printStackTrace();
			FacesUtil.addErrorMessage("Não foi possível salvar o Categoria.");
		} 
	}
	
	public void excluir(){
		try {
			if(ehSelectedNode()){
				
				getService().removePorId(getDataFromSelectedNode(getSelectedNode()).getId());
				
				getSelectedNode().getChildren().clear();
				getSelectedNode().getParent().getChildren().remove(getSelectedNode());
				getSelectedNode().setParent(null);
				setSelectedNode(null);
			} else {
				FacesUtil.addErrorMessage("Nenhum Categoria selecionado.");
			}
			FacesUtil.addInfoMessage("Registro excluido com sucesso."); 
		} catch (ServiceException e) {
			e.printStackTrace();
			FacesUtil.addErrorMessage("Não foi possível excluir o Categoria.");
		} 
	}
	
	public void visualizar(){
		if(ehSelectedNode()){
			this.setCategoriaSelecionado(this.getDataFromSelectedNode(getSelectedNode()));
			final RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgCategoria').show();");
		} else {
			FacesUtil.addErrorMessage("Nenhum Categoria selecionado.");
		}
	}
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public Categoria getCategoriaSelecionado() {
		return categoriaSelecionado;
	}

	public void setCategoriaSelecionado(Categoria CategoriaSelecionado) {
		this.categoriaSelecionado = CategoriaSelecionado;
	}


}
