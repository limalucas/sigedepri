package br.com.sigedepri.controllers.categoria;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.sigedepri.controllers.AbstractFormBean;
import br.com.sigedepri.dao.CategoriaDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.PalavraChave;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.jsf.FacesUtil;


@Named
@ViewScoped
public class CadastroCategoria extends AbstractFormBean<Integer, Categoria, CategoriaDAO, CategoriaService>{

	private static final long serialVersionUID = 1L;
	
	private Categoria categoria;
	
	private PalavraChave palavraChave;
	
	private List<Categoria> categorias;
	
	@PostConstruct
	public void init(){
		if (FacesUtil.isNotPostback()) {
			limpaFormulario();
		}
		
		if (FacesUtil.hasFlashParameter("categoria")) {
			this.setCategoria((Categoria) FacesUtil.getFlashParameter("categoria"));
		}
	}
	
	public void limpaFormulario(){
		this.setCategoria(new Categoria());
		this.setPalavraChave(new PalavraChave()); 
	}
	
	public String salvar(){
		try {
			
			getService().salva(this.getCategoria());
			
			FacesUtil.setKeepMessages(true);
			FacesUtil.addInfoMessage("Registro salvo com sucesso!");
			limpaFormulario(); 
			return "pesquisaCategoria?faces-redirect=true";
		
		} catch (ServiceException e) {  
			e.printStackTrace();
			FacesUtil.addErrorMessage("Não foi possível salvar a categoria!");
			return "";
		} 
	}
	
	public String adicionarPalavraChave() {
		this.palavraChave.setCategoria(this.categoria); 
		this.categoria.getPalavrasChaves().add(palavraChave);
		this.setPalavraChave(new PalavraChave()); 
        return null;
    } 

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Categoria> getCategorias() {
		if(categorias == null){
			this.categorias = getService().listaCategoriasPorUsuario();
		}
		return categorias;
	}

	public PalavraChave getPalavraChave() {
		return palavraChave;
	}

	public void setPalavraChave(PalavraChave palavraChave) {
		this.palavraChave = palavraChave;
	}

}
