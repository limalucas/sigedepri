package br.com.sigedepri.controllers;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.sigedepri.entity.Usuario;
import br.com.sigedepri.security.LoginFacebook;
import br.com.sigedepri.security.UsuarioFacebook;
import br.com.sigedepri.service.UsuarioService;
import br.com.sigedepri.util.excpetion.ServiceException;


@WebServlet("/loginfbresponse")
public class LoginFaceResponse extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final String CODE = "code";
	
	@Inject
	private LoginFacebook loginFacebook;
	
	@Inject
	private UsuarioService usuarioService;

	@Override
	protected void service( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
		try {
			final String code = request.getParameter(CODE);

			//Verifica se o token veio corretamente.
			if( code == null || code.isEmpty() ){
				throw new IllegalArgumentException( "Não foi possível obter o code de integração com o facebook!" );
			}
			
			UsuarioFacebook usuarioFacebook = loginFacebook.obterUsuarioFacebook(code);
			
			Usuario usuarioLogadoSalvo = salvaUsuarioFacebookNoBanco(usuarioFacebook);
			
			request.getSession().setAttribute("usuarioLogado", usuarioLogadoSalvo);
			
			response.sendRedirect( response.encodeRedirectURL(request.getContextPath() + "/home.xhtml") );
			
		} catch ( final Exception e ) {
			e.printStackTrace();
			throw new IllegalArgumentException( "Não foi possível fazer a integração com o facebook " + e.getMessage() );
		}
	}
	
	private Usuario salvaUsuarioFacebookNoBanco(final UsuarioFacebook userFb){
		
		try {
			Usuario usuarioLogado = new Usuario(userFb);		
			return usuarioService.salva(usuarioLogado);
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new IllegalArgumentException( "Não foi possível salvar o usuário " + userFb.getName() + " no banco de dados " + e.getMessage() );
		}
		
	}

}
