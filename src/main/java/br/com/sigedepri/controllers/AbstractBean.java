package br.com.sigedepri.controllers;

import java.io.Serializable;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import br.com.sigedepri.dao.AbstractJpaDao;
import br.com.sigedepri.dao.IDao;
import br.com.sigedepri.entity.AbstractEntity;
import br.com.sigedepri.service.AbstractService;


public abstract class AbstractBean<K, E extends AbstractEntity<K>, D extends IDao<K, E>, S extends AbstractService<K, E, D>> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Instance<AbstractService<K, E, D>> service;
	
	@Inject
	private Instance<AbstractJpaDao<K, E>> dao;
	
	@SuppressWarnings("unchecked")
	protected S getService() {
		return (S) service.get();
	}
	
	@SuppressWarnings("unchecked")
	protected D getDao() {
		return (D) dao.get();
	}

}