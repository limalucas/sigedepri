package br.com.sigedepri.controllers.transacao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.com.sigedepri.controllers.AbstractBean;
import br.com.sigedepri.dao.TransacaoDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.service.TransacaoService;
import br.com.sigedepri.util.Formatador;
import br.com.sigedepri.util.excpetion.CoreException;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroTransacaoEmLoteBean extends AbstractBean<Integer, Transacao, TransacaoDAO, TransacaoService> {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CategoriaService categoriaService;
	
	private List<Categoria> categoriasPai;
	private List<Categoria> subCategorias;
	private Categoria categoriaPaiSelecionada;
	
	private boolean selecionaTodos;
	
	private List<Transacao> transacoesEmLote;
	private Transacao transacaoSelecionada;

	private UploadedFile arquivoCarregado; 
	
	@PostConstruct
	public void init(){
		System.out.println("Iniciando cadastro transação em lote");
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		try {
			this.arquivoCarregado = event.getFile();
			this.transacoesEmLote = getService().carregarArquivoEmLote(event.getFile().getInputstream());
		} catch (IOException | ServiceException e) { 
			e.printStackTrace();
			FacesUtil.addErrorMessage("Não foi possível carregar o arquivo " + event.getFile().getFileName()); 
		} 
    }
	
	public String calculaValorTotal(){
		BigDecimal total = BigDecimal.ZERO;
		if(transacoesEmLote != null && transacoesEmLote.size() > 0){
			for (Transacao transacao : transacoesEmLote) {
				total = total.add(transacao.getValor());
			}
		}
		return Formatador.formataNumero(total);
	}
	
	public void salvarPorLinha(Transacao transacaoAtual) throws CoreException {
		try {
			int indexOf = getTransacoesEmLote().indexOf(transacaoAtual);
			transacaoAtual = getService().salva(transacaoAtual);
			getTransacoesEmLote().set(indexOf, transacaoAtual);
			FacesUtil.addInfoMessage("Registro salvo com sucesso!");
		} catch (ServiceException e) {
			throw new CoreException(e.getCause().getMessage());
		} catch (Exception e) {
			throw new CoreException("Não foi possível salvar a transação " + transacaoAtual.getDescricao());
		}
	}
	
	public void selecionarTodos(){
		if(this.getTransacoesEmLote() != null && !this.getTransacoesEmLote().isEmpty()){
			for (Transacao transacao : this.getTransacoesEmLote()) {
				if(selecionaTodos){
					transacao.setSelecionado(true);
				}else {
					transacao.setSelecionado(false);
				}
			}
		}
	}

	public void onChangeCategoriaPai(Transacao transacaoAtual){
		transacaoAtual.setSubCategorias(this.categoriaService.listaSubCategorias(transacaoAtual.getCategoriaPai()));
	}

	public List<Categoria> getCategoriasPai() {
		if(categoriasPai == null){
			this.categoriasPai = categoriaService.listaCategoriasPai();
		}
		return categoriasPai;
	}

	public List<Categoria> getSubCategorias() {
		return subCategorias;
	}

	public Categoria getCategoriaPaiSelecionada() {
		return categoriaPaiSelecionada;
	}

	public void setCategoriaPaiSelecionada(Categoria categoriaPaiSelecionada) {
		this.categoriaPaiSelecionada = categoriaPaiSelecionada;
	}

	public void setSubCategorias(List<Categoria> subCategorias) {
		this.subCategorias = subCategorias;
	}

	public List<Transacao> getTransacoesEmLote() {
		return transacoesEmLote;
	}

	public void setTransacoesEmLote(List<Transacao> transacoesEmLote) {
		this.transacoesEmLote = transacoesEmLote;
	}

	public boolean isSelecionaTodos() {
		return selecionaTodos;
	}

	public void setSelecionaTodos(boolean selecionaTodos) {
		this.selecionaTodos = selecionaTodos;
	}

	public Transacao getTransacaoSelecionada() {
		return transacaoSelecionada;
	}

	public void setTransacaoSelecionada(Transacao transacaoSelecionada) {
		this.transacaoSelecionada = transacaoSelecionada;
	}

	public UploadedFile getArquivoCarregado() {
		return arquivoCarregado;
	}

	public void setArquivoCarregado(UploadedFile arquivoCarregado) {
		this.arquivoCarregado = arquivoCarregado;
	}
	
	public void removeTodosSelecionados(){
		List<Transacao> transacaoToRemove = new ArrayList<>();
		for ( Transacao transacao : getTransacoesEmLote() ) {
			if( transacao.isSelecionado() ){
				transacaoToRemove.add( transacao ); 
			}
		}
		getTransacoesEmLote().removeAll( transacaoToRemove ); 
	}
	
	public void editar(Transacao transacao){
		transacao.setEditando( true ); 
	}

}
