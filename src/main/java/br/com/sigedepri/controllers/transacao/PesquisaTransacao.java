package br.com.sigedepri.controllers.transacao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.data.FilterEvent;

import br.com.sigedepri.controllers.AbstractFilterBean;
import br.com.sigedepri.dao.CategoriaDAO;
import br.com.sigedepri.dao.TransacaoDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.FiltroTransacao;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.service.TransacaoService;
import br.com.sigedepri.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaTransacao extends AbstractFilterBean<Integer, Transacao, TransacaoDAO, TransacaoService> {

	private static final long serialVersionUID = 1L;
	
	private List<Categoria> categorias;
	private List<Categoria> subCategorias;
	
	@Inject
	private CategoriaDAO categoriaDAO;
	
	private FiltroTransacao filtroTransacao;
	
	@PostConstruct
	public void init() {
		if ( FacesUtil.isNotPostback() ) {
			if(FacesUtil.hasFlashParameter("keepFiltro")){ 
				filtroTransacao = FacesUtil.getFlashParameter("keepFiltro");
				RequestContext requestContext = RequestContext.getCurrentInstance();  
				requestContext.execute("PF('transacaoTable').filter()"); 
			} else {
				filtroTransacao = new FiltroTransacao();
			}
		}
	}
	
	@Override
	public String editar() {
		FacesUtil.addFlashParameter("keepFiltro", getFiltroTransacao());
		return super.editar();
	}
	
	public void novoFiltro(){
		filtroTransacao = new FiltroTransacao();
	}
	
	public void onFilterChange(FilterEvent filterEvent) {
        filterEvent.getFilters().put(FiltroTransacao.TRANSACAO_FILTER, filtroTransacao);
    }

	public List<Categoria> getCategorias() {
		if(categorias == null){
			categorias = categoriaDAO.listaCategoriasPai();	
		}
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public FiltroTransacao getFiltroTransacao() {
		return filtroTransacao;
	}

	public void setFiltroTransacao(FiltroTransacao filtroTransacao) {
		this.filtroTransacao = filtroTransacao;
	}

	public List<Categoria> getSubCategorias() {
		if( filtroTransacao.getCategoria() != null  ){
			this.subCategorias = categoriaDAO.listaSubCategorias(filtroTransacao.getCategoria()); 
		}
		return subCategorias;
	}

	public void setSubCategorias(List<Categoria> subCategorias) {
		this.subCategorias = subCategorias;
	}

}
