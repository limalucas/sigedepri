package br.com.sigedepri.controllers.transacao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sigedepri.controllers.AbstractFormBean;
import br.com.sigedepri.dao.TransacaoDAO;
import br.com.sigedepri.entity.Categoria;
import br.com.sigedepri.entity.transacao.FiltroTransacao;
import br.com.sigedepri.entity.transacao.Transacao;
import br.com.sigedepri.service.CategoriaService;
import br.com.sigedepri.service.TransacaoService;
import br.com.sigedepri.util.excpetion.ServiceException;
import br.com.sigedepri.util.jsf.FacesUtil;


@Named
@ViewScoped
public class CadastroTransacao extends AbstractFormBean<Integer, Transacao, TransacaoDAO, TransacaoService> {

	private static final long serialVersionUID = 1L;

	private Transacao transacao;
	
	@Inject
	private CategoriaService categoriaService;
	
	private List<Categoria> categoriasPai;
	private List<Categoria> subCategorias;
	
	private FiltroTransacao filtro;

	@PostConstruct
	public void init() {

		if (FacesUtil.isNotPostback()) {
			limpaFormulario();
		}
		
		if (FacesUtil.hasFlashParameter("keepFiltro")) {
			filtro = FacesUtil.getFlashParameter("keepFiltro");
		}

		if (FacesUtil.hasFlashParameter("transacao")) {
			this.setTransacao((Transacao) FacesUtil.getFlashParameter("transacao"));
			
			if(this.getTransacao().getCategoria() != null && this.getTransacao().getCategoria().getCategoriaPai() != null){
				this.getTransacao().setCategoriaPai( this.getTransacao().getCategoria().getCategoriaPai() );
			}
			
			onChangeCategoriaPai(); 
		}
	
	}
	
	public boolean isEditando(){
		return this.getTransacao().getId() != null;
	}
	
	public String salvar() {
		try {
			getService().salva(this.getTransacao());
			
			FacesUtil.setKeepMessages(true);
			FacesUtil.addInfoMessage("Registro salvo com sucesso!");
			limpaFormulario();
			if (FacesUtil.hasFlashParameter("keepFiltro")) {
				FacesUtil.addFlashParameter("keepFiltro", getFiltro());
			}
			return "pesquisaTransacao?faces-redirect=true";

		} catch (ServiceException e) {
			e.printStackTrace();
			FacesUtil.addErrorMessage("Não foi possível salvar a transação! " + e.getCause().getMessage());
			return "";
		}
	}
	
	public void limpaFormulario() {
		this.setTransacao(new Transacao());
	}
	
	public void onChangeCategoriaPai(){
		this.setSubCategorias(this.categoriaService.listaSubCategorias(this.getTransacao().getCategoriaPai()));
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}

	public List<Categoria> getCategoriasPai() {
		if(categoriasPai == null){
			this.categoriasPai = categoriaService.listaCategoriasPai();
		}
		return categoriasPai;
	}

	public void setCategoriasPai(List<Categoria> categoriasPai) {
		this.categoriasPai = categoriasPai;
	}

	public List<Categoria> getSubCategorias() {
		return subCategorias;
	}

	public void setSubCategorias(List<Categoria> subCategorias) {
		this.subCategorias = subCategorias;
	}

	public FiltroTransacao getFiltro() {
		return filtro;
	}

	public void setFiltro(FiltroTransacao filtro) {
		this.filtro = filtro;
	}
	
}
