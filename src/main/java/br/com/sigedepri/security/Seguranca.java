package br.com.sigedepri.security;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sigedepri.annotation.UsuarioLogado;
import br.com.sigedepri.entity.Usuario;

@Named
@SessionScoped
public class Seguranca implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioLogado;

	@Produces
	@UsuarioLogado
	public Usuario getUsuarioLogado() {
		if(usuarioLogado == null){
			FacesContext context = FacesContext.getCurrentInstance();
			return (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");
		}
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

}
