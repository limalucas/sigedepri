package br.com.sigedepri.security;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.json.JSONException;
import org.json.JSONObject;

@Named
@RequestScoped
public class LoginFacebook implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public UsuarioFacebook obterUsuarioFacebook(String code) throws MalformedURLException, IOException, JSONException { 

		String retorno = readURL(new URL(this.getAuthURL(code)));
		
		JSONObject jsonobj = new JSONObject(retorno);
		
		String accessToken = (String) jsonobj.get( "access_token" );  
		/*@SuppressWarnings("unused")
		Integer expires = null;
		String[] pairs = retorno.split(",");
		for (String pair : pairs) {
			String[] kv = pair.split("=");
			if (kv.length != 2) {				
				throw new RuntimeException("Resposta auth inesperada.");				
			} else {
				if (kv[0].equals("access_token")) {
					accessToken = kv[1];
				}
				if (kv[0].equals("expires")) {
					expires = Integer.valueOf(kv[1]);
				}
			}
		}*/

		JSONObject resp = new JSONObject(readURL(new URL("https://graph.facebook.com/me?fields=picture,name,id,first_name,last_name,gender,email,timezone,age_range,verified&access_token=" + accessToken)));

		UsuarioFacebook usuarioFacebook = new UsuarioFacebook(resp);
		
		return usuarioFacebook;

	}
	
	private String readURL(URL url) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = url.openStream();
		int r;
		while ((r = is.read()) != -1) {
			baos.write(r);
		}
		return new String(baos.toByteArray());
	}


	public String getLoginRedirectURL() {
		return "https://graph.facebook.com/oauth/authorize?client_id="
				+ DadosDaAppNoFacebook.getCLIENT_ID() + "&display=page&redirect_uri=" + DadosDaAppNoFacebook.getREDIRECT_URI()
				+ "&scope=email,publish_actions";
	}

	public String getAuthURL(String authCode) {
		return "https://graph.facebook.com/oauth/access_token?client_id="
				+ DadosDaAppNoFacebook.getCLIENT_ID() + "&redirect_uri=" + DadosDaAppNoFacebook.getREDIRECT_URI()
				+ "&client_secret=" + DadosDaAppNoFacebook.getCLIENT_SECRET() + "&code=" + authCode;
	}
}
