package br.com.sigedepri.security;

import br.com.sigedepri.enuns.EnvironmentType;
import br.com.sigedepri.util.env.EnvironmentUtil;

/**
 * Configure aqui os dados da sua aplicacao no facebook.
 * 
 * @author Lucas Lima
 */
public class DadosDaAppNoFacebook {

	/**
	 * Coloque aqui a chave da app do facebook
	 */
	private static final String CLIENT_SECRET = "709463fbefdb2cbf78f1c70cc848340e";
	private static final String CLIENT_SECRET_LOCAL = "86d1051374c76dbf4c36ff7c9e3aaa90";

	/**
	 * Coloque aqui o id da app do facebook
	 */
	private static final String CLIENT_ID = "887281818073608";
	private static final String CLIENT_ID_LOCAL = "711269352357121";

	/**
	 * Coloque aqui a url do servidor q voce esta rodando esse projeto
	 */
	private static final String REDIRECT_URI = "http://financeiro-luclima.rhcloud.com/Sigedepri/loginfbresponse";
	private static final String REDIRECT_URI_LOCAL = "http://localhost:8080/Sigedepri/loginfbresponse";

	
	
	public static String getCLIENT_SECRET() {
		return EnvironmentUtil.getCurrentEviriomentType().equals(EnvironmentType.LOCAL) ? CLIENT_SECRET_LOCAL : CLIENT_SECRET;
	}

	public static String getCLIENT_ID() {
		return EnvironmentUtil.getCurrentEviriomentType().equals(EnvironmentType.LOCAL) ? CLIENT_ID_LOCAL : CLIENT_ID;
	}

	public static String getREDIRECT_URI() {
		return EnvironmentUtil.getCurrentEviriomentType().equals(EnvironmentType.LOCAL) ? REDIRECT_URI_LOCAL : REDIRECT_URI;
	}

}
