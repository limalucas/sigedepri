package br.com.sigedepri.enuns;

public enum TipoTransacao {

	DEBITO("D", "Débito"), CREDITO("C", "Crédito");

	private String valor;
	private String descricao;

	private TipoTransacao(String valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public String getValorString() {
		return valor.toString();
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoTransacao fromString(String requisicao) {
		if (requisicao != null) {
			for (TipoTransacao valorAtual : TipoTransacao.values()) {
				if (requisicao.equals(valorAtual.getValor())) {
					return valorAtual;
				}
			}
		}
		return null;
	}

}
