package br.com.sigedepri.enuns;

import java.io.Serializable;

public enum AtivoInativo implements Serializable{

	ATIVO("A", "Ativo"), INATIVO("I", "Inativo");

	private String valor;
	private String descricao;

	private AtivoInativo(String valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public static AtivoInativo fromString(String requisicao) {
		if (requisicao != null) {
			for (final AtivoInativo valorAtual : AtivoInativo.values()) {
				if (requisicao.equals(valorAtual.getValor())) {
					return valorAtual;
				}
			}
		}
		return null;
	}

}
