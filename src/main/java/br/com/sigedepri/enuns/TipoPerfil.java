package br.com.sigedepri.enuns;

public enum TipoPerfil {
	
	ADMINISTRADOR("A", "Administrador do Sistema"), CONVIDADO("C", "Convidado"), GESTOR_FINANCEIRO("G", "Gestor Financeiro");

	private String valor;
	private String descricao;

	private TipoPerfil(String valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoPerfil fromString(String requisicao) {
		if (requisicao != null) {
			for (final TipoPerfil valorAtual : TipoPerfil.values()) {
				if (requisicao.equals(valorAtual.getValor())) {
					return valorAtual;
				}
			}
		}
		return null;
	}

}
