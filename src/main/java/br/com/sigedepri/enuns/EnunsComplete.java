package br.com.sigedepri.enuns;

import javax.inject.Named;

@Named
public class EnunsComplete {
	
	public TipoTransacao[] tipoTransacao() {
		return TipoTransacao.values();
	}
	
	public TipoConsulta[] tipoConsulta() {
		return TipoConsulta.values();
	}

}
